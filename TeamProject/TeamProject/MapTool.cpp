#include <glut.h> 
#include<stdio.h>
#include<windows.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
//가운 데 대관람차
//왼쪽 뒤에 자이로드롭
//왼쪽 앞에 바이킹
//오른쪽 앞에 롤러코스터
//오른쪽 뒤에 회전목마
////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma warning(disable:4996)
GLvoid drawScene(GLvoid);
GLvoid reshape(int w, int h);
void SpecialKeyboard(int key, int x, int y);
void Keyboard(unsigned char key, int x, int y);
void Mouse(int button, int state, int x, int y);
void Motion(int x, int y);
void Timer(int value);
void BoyTimer(int value);
void DrawFloor();//바닥 그리기 
void DrawGyroDrop();//자이로 드롭
void DrawViking();//바이킹
void DrawBigWheel();//대관람차
void SquareTower(GLfloat centerx, GLfloat centery, GLfloat centerz, GLfloat radius, GLfloat h);
void BoyPerson(void);
void DrawHorse(void);
void DrawMerryGoRound(void);
////////////////////////////////////////////////////////////////////////////////////////////////////
#define GL_PI 3.1415f
float radian = GL_PI / 180.0f;
float viewx = 0.0;//x
float viewy = 0.0;//y
float viewz = 10;//z
float rotate_x = 0.0;
float rotate_y = 0.0;
float rotate_z = 0.0;
int downx, downy;
bool LeftButtonDown = false;
bool RightButtonDown = false;
struct mouse {
	double x1 = 0;
	double x2 = 0;
	double x3 = 0;
	double y1 = 0;
	double y2 = 0;
	double y3 = 0;
	int judge = 0;
	bool tf1 = false;
	bool tf2 = false;
};
struct keyoboard {
	float x1 = -340.0;
	float y1 = 340.0;
	float z1 = 340.0;
	int judge = 0;
	bool tf1 = false;
	bool tf2 = false;
};

int ctrol_point_counter = 1;
bool ctrol_point_shape_tf[10] = { 0 };
int ctrol_point_tf = 0;
int ctrol_shape = 1;
int ctrol_shape2 = 0;
int sijum1 = 0;
struct star {
	float x = 0.0;
	float y = 0.0;
	float z = 0.0;
	float dirx = 0.0;
	float diry = 0.0;
	float dirz = 0.0;
	float colorr = 0.0;
	float colorb = 0.0;
	float colorg = 0.0;
	float movex = 0.0;
	float movey = 0.0;
	float movez = 0.0;
	bool tf = false;
	int dir = 0; // 회전 방향값
	float size = 0.0;
};
struct shape {
	double plus = rand() % 10 + 1;
	double scale = 0.05;
	// 회전값
	double rotx = 0;
	double roty = 0;
	double rotz = 0;
	// 이동값
	double tranx = 0;
	double trany = 0;
	double tranz = 0;
	// 이동
	double scalx = 0;
	double scaly = 0;
	double scalz = 0;
	double angle = 0;
	int count = 0;
	int dir = 0;
	int self_dir = 0;
	bool tf = 0;
};
shape GyroDrop;//자이로드롭
shape Viking;//바이킹
shape BigWheel;//대관람차
shape person;//사람
shape MerryGoRound;
shape person_head;
shape person_lhand_up;
shape person_lhand_down;
shape person_rhand_up;
shape person_rhand_down;
shape person_lleg_up;
shape person_lleg_down;
shape person_rleg_up;
shape person_rleg_down;
void Pointer();
keyoboard key2;
mouse mouse1;
shape point;
//롤러코스터 변수들 *********************************************************************
GLfloat ctrlpoints[100][6][3] = { { 0 } }; //베지에 정점 6개 설정
star ctrol_point[400];
shape rollercoaster;
void RollerCoaster_Base();
void roller_point_set(); //메인 함수에 작성
						 ////////////////////////////////////////////////////////////////////////////////////////////////////
GLvoid drawScene()                 // 출력 함수 
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);          // 바탕색을 'blue' 로 지정      
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
	glLoadIdentity();
	glTranslatef(0, -1000, viewz);
	glRotatef(viewx, 0, 1, 0);
	glRotatef(viewy, -1, 0, 0);
	glRotatef(point.rotx, 1.0, 0.0, 0.0);
	glRotatef(point.roty, 0.0, 1.0, 0.0);
	glRotatef(point.rotz, 0.0, 0.0, 1.0);
	Pointer();
	DrawFloor();
	RollerCoaster_Base();
	glutSwapBuffers();
}
star ctrol_point1[400];
////////////////////////////////////////////////////////////////////////////////////////////////////
GLvoid SpecialKeyboard(int key, int x, int y)
{
	if (key == GLUT_KEY_LEFT) {
		if (person_head.tf == false) {
			person.roty = 270;
			person.tranx -= 5;
			person.tf = true;
		}
		else {
			if (person.roty == 0) {
				person.roty = 90;
			}
			else if (person.roty == 90) {
				person.roty = 180;
			}
			else if (person.roty == 180) {
				person.roty = 270;
			}
			else if (person.roty == 270) {
				person.roty = 0;
			}
		}
	}
	if (key == GLUT_KEY_RIGHT) {
		if (person_head.tf == false) {
			person.roty = 90;
			person.tranx += 5;
			person.tf = true;
		}
		else {
			if (person.roty == 0) {
				person.roty = 270;
			}
			else if (person.roty == 90) {
				person.roty = 0;
			}
			else if (person.roty == 180) {
				person.roty = 90;
			}
			else if (person.roty == 270) {
				person.roty = 180;
			}
		}
	}
	if (key == GLUT_KEY_UP) {
		if (person_head.tf == false) {
			person.roty = 180;
			person.tranz -= 5;
			person.tf = true;
			printf("^");
		}
		else {
			if (person.roty == 0) {
				person.tranz -= 5;
			}
			else if (person.roty == 90) {
				person.tranx += 5;
			}
			else if (person.roty == 180) {
				person.tranz += 5;
			}
			else if (person.roty == 270) {
				person.tranx -= 5;
			}
		}
	}
	if (key == GLUT_KEY_DOWN) {
		if (person_head.tf == false) {
			person.roty = 0;
			person.tf = true;
		}
		else {
			if (person.roty == 0) {
				person.tranz += 5;
			}
			else if (person.roty == 90) {
				person.tranx -= 5;
			}
			else if (person.roty == 180) {
				person.tranz -= 5;
			}
			else if (person.roty == 270) {
				person.tranx += 5;
			}
		}
	}
}
void Keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case '1': {
		point.roty = 0;
		break;
	}
	case '2': {
		point.roty = 90;
		break;
	}
	case '3': {
		point.roty = 180;
		break;
	}
	case'4': {
		point.roty = 270;
		break;
	}
	case '5': {
		person.count++;
		if (person.count % 2 == 1) {
			person_head.tf = true;
		}
		else {
			person_head.tf = false;
		}
		break;
	}
	case 'q': case'Q': {
		if (key2.x1 > -4500) {
			key2.x1 -= 10;
		}
		break;
	}
	case 'a': case'A': {
		if (key2.x1 < 4500) {
			key2.x1 += 10;
		}
		break;
	}
	case 'w': case'W': {
		if (key2.y1 < 4500) {
			key2.y1 += 10;
		}
		break;
	}
	case 's': case'S': {
		if (key2.y1 > -4500) {
			key2.y1 -= 10;
		}
		break;
	}
	case 'e': case'E': {
		if (key2.z1 > -4500) {
			key2.z1 -= 10;
		}
		break;
	}
	case 'd': case'D': {
		if (key2.z1 < 4500) {
			key2.z1 += 10;
		}
		break;
	}
	case 'r': case'R': {
		// 정점 찍기
		ctrol_point[ctrol_point_counter].x = key2.x1;
		ctrol_point[ctrol_point_counter].y = key2.y1;
		ctrol_point[ctrol_point_counter].z = key2.z1;

		printf("%f, %f, %f \n", ctrol_point[ctrol_point_counter].x, ctrol_point[ctrol_point_counter].y, ctrol_point[ctrol_point_counter].z);
		for (int i = ctrol_shape; i < 6; i++) {
			ctrlpoints[ctrol_shape2][i][0] = ctrol_point[ctrol_point_counter].x;
			ctrlpoints[ctrol_shape2][i][1] = ctrol_point[ctrol_point_counter].y;
			ctrlpoints[ctrol_shape2][i][2] = ctrol_point[ctrol_point_counter].z;
		}
		if (ctrol_shape == 5) {
			for (int i = 0; i < 6; i++) {
				ctrlpoints[ctrol_shape2 + 1][i][0] = ctrlpoints[ctrol_shape2][5][0];
				ctrlpoints[ctrol_shape2 + 1][i][1] = ctrlpoints[ctrol_shape2][5][1];
				ctrlpoints[ctrol_shape2 + 1][i][2] = ctrlpoints[ctrol_shape2][5][2];
			}
		}

		ctrol_shape++;
		ctrol_point_counter++;
		if (ctrol_shape == 6) {
			ctrol_shape = 1;
			ctrol_shape2++;
		}
		break;
	}
	case 't': {
		if (ctrol_point_counter > 0) {
			ctrol_point_counter--;
			printf("ctrol_point_counter : %d \n", ctrol_point_counter);
			printf("ctrol_shape : %d \n", ctrol_shape);
			printf("ctrol_shape2 : %d \n", ctrol_shape2);
			// 0일때 전단계 4로 가야한다.
			if (ctrol_shape == 1) {
				ctrol_point_shape_tf[ctrol_shape2] = false;
				ctrol_shape = 5;
				ctrol_shape2--;
			}
			if (ctrol_shape > 1) {
				printf("1111 \n");
				ctrol_shape--;
				for (int i = ctrol_shape; i < 6; i++) {
					ctrlpoints[ctrol_shape2][i][0] = ctrol_point[ctrol_point_counter - 1].x;
					ctrlpoints[ctrol_shape2][i][1] = ctrol_point[ctrol_point_counter - 1].y;
					ctrlpoints[ctrol_shape2][i][2] = ctrol_point[ctrol_point_counter - 1].z;
				}
			}
			if (ctrol_point_counter == 0) {
				printf("2222 \n");
				ctrol_point_counter = 1;
				ctrol_shape = 1;
				ctrol_shape2 = 0;
				for (int p = 0; p < 6; p++) {
					ctrlpoints[0][p][0] = 4500;
					ctrlpoints[0][p][1] = 500;
					ctrlpoints[0][p][2] = 2000;
				}
				ctrol_point_shape_tf[0] = true;
			}
		}
		break;
	}
	case 'm': {
		FILE *f;
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 6; j++) {
				f = fopen("b.txt", "a");
				fprintf(f, "%f %f %f \n", ctrlpoints[i][j][0], ctrlpoints[i][j][1], ctrlpoints[i][j][2]);
				fclose(f);
			}
		}

		break;
	}
	case 'n': {
		FILE *f;
		f = fopen("b.txt", "w");
		fprintf(f, "");
		fclose(f);
		break;
	}
	case 'b': {
		FILE *f;
		f = fopen("b.txt", "r");
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 6; j++) {
				fscanf(f, "%f %f %f", &ctrlpoints[i][j][0], &ctrlpoints[i][j][1], &ctrlpoints[i][j][2]);
			}
		}
		fclose(f);
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 6; j++) {
				printf("%f %f %f \n", ctrlpoints[i][j][0], ctrlpoints[i][j][1], ctrlpoints[i][j][2]);
			}
		}

		break;
	}
	}
	glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void main(int argc, char *argv[]) {
	srand((unsigned)time(NULL));
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(200, 70);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Points Drawing");
	roller_point_set();
	glutKeyboardFunc(Keyboard);
	glutSpecialFunc(SpecialKeyboard);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);
	glutDisplayFunc(drawScene);
	glutReshapeFunc(reshape);
	glutMainLoop();
}
////////////////////////////////////////////////////////////////////////////////////////////////////
GLvoid reshape(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-6000, 6000, -6000, 6000, -6000, 6000);
	//gluPerspective(60.0, w / h, 100.0, 10000.0);
	//glTranslatef(0.0, 0.0, -6000);
}
////////////////////////////////////////////////////////////////////////////////////////////////////

int motion_button = 0; // 눌렸는지 판정
int motion_state = 0; // 상태 판정
double size_down = 0;
double size_up = 0;
int move_motion = 0;
double mouse_point = 0;
double pointer = 0;
int motion_handle = 0;
double slope_mouse = 0; // 마우스 기울기
void Mouse(int button, int state, int x, int y) {
	int motion_button = button;
	int state_button = state;
	slope_mouse = 0;
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		mouse1.tf2 = false;
		mouse1.x1 = x;
		mouse1.y1 = y;
		mouse1.tf1 = true;
	}
	if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
		mouse1.x2 = x;
		mouse1.y2 = y;
		mouse1.tf2 = true;
		pointer = sqrt(((mouse1.x2 - mouse1.x1) * (mouse1.x2 - mouse1.x1)) + ((mouse1.y2 - mouse1.y1) * (mouse1.y2 - mouse1.y1)));
		mouse_point = (int)(pointer) / 200; // 마우스 포인터 rotz 값
		slope_mouse = (mouse1.y2 - mouse1.y1) / (mouse1.x2 - mouse1.x1);
		if (slope_mouse >= -1 && slope_mouse <= 1) {
			if (mouse1.tf1 == true) {
				if (mouse1.x2 < mouse1.x1) {
					point.roty -= mouse_point;
					if (point.roty <= -360) {
						point.roty = 0;
					}
				}
				else if (mouse1.x2 > mouse1.x1) {
					point.roty += mouse_point;
					if (point.roty >= 360) {
						point.roty = 0;
					}
				}
			}
		}
		else if (slope_mouse < -1 || slope_mouse > -1) {
			if (mouse1.tf1 == true) {
				if (mouse1.y2 < mouse1.y1) {
					point.rotx -= mouse_point;
					if (point.rotx <= -360) {
						point.rotx = 0;
					}
				}
				else if (mouse1.y2 > mouse1.y1) {
					point.rotx += mouse_point;
					if (point.rotx >= 360) {
						point.rotx = 0;
					}
				}
			}
		}
	}
	glutPostRedisplay();
}
void Motion(int x, int y) {
	slope_mouse = 0;
	if (motion_button == GLUT_LEFT_BUTTON) {
		mouse1.x2 = x;
		mouse1.y2 = y;
		pointer = sqrt(((mouse1.x2 - mouse1.x1) * (mouse1.x2 - mouse1.x1)) + ((mouse1.y2 - mouse1.y1) * (mouse1.y2 - mouse1.y1)));
		mouse_point = (int)(pointer) / 200;
		slope_mouse = (mouse1.y2 - mouse1.y1) / (mouse1.x2 - mouse1.x1);
		if (mouse1.x2 - mouse1.x1 == 0) {
			slope_mouse = 5;
		}
		if (slope_mouse >= -1 && slope_mouse <= 1) {
			if (mouse1.tf1 == true) {
				if (mouse1.x2 < mouse1.x1) {
					point.roty -= mouse_point;
					if (point.roty <= -360) {
						point.roty = 0;
					}
				}
				else if (mouse1.x2 > mouse1.x1) {
					point.roty += mouse_point;
					if (point.roty >= 360) {
						point.roty = 0;
					}
				}
			}
		}
		else if (slope_mouse < -1 || slope_mouse > -1) {
			if (mouse1.tf1 == true) {
				if (mouse1.y2 < mouse1.y1) {
					point.rotx -= mouse_point;
					if (point.rotx <= -360) {
						point.rotx = 0;
					}
				}
				else if (mouse1.y2 > mouse1.y1) {
					point.rotx += mouse_point;
					if (point.rotx >= 360) {
						point.rotx = 0;
					}
				}
			}
		}
	}
	glutPostRedisplay();
}
//////////////////////////////////////////////바닥////////////////////////////////////////////////////
void DrawFloor(void)
{
	glPushMatrix();
	glColor3f(0.2, 0.8, 0.8);
	glTranslatef(0, -500, 0);
	glRotatef(90, 1, 0, 0);
	glScalef(1, 1, 0.1);
	glutSolidCube(10000);
	glPopMatrix();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////=

void roller_point_set() {
	ctrlpoints[ctrol_shape2][0][0] = 4500;
	ctrlpoints[ctrol_shape2][0][1] = 500;
	ctrlpoints[ctrol_shape2][0][2] = 2000;

	key2.x1 = 4500;
	key2.y1 = 500;
	key2.z1 = 2000;
}





//////롤러코스터///////////////////////////////
void RollerCoaster_Base() {
	glPushMatrix();
	glPointSize(20.0);
	glLineWidth(4.0);
	/////////////////////////////// 베지에 곡선
	for (int ctrl = 0; ctrl < 100; ctrl++) {
		glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 6, &ctrlpoints[ctrl][0][0]);
		glEnable(GL_MAP1_VERTEX_3);
		// 선그리기
		glColor3f(1, 1, 0);
		glBegin(GL_LINE_STRIP);
		for (int line = 0; line < 500; line++) {
			glEvalCoord1f((GLfloat)line / 500);
		}
		glEnd();
		glDisable(GL_MAP1_VERTEX_3);
	}

	//정점 그리기
	glPointSize(10.0);
	glColor3f(1.0, 0.5, 1.0);
	glBegin(GL_POINTS);
	for (int b = 0; b < 100; b++) {
		for (int a = 0; a < 6; a++) {
			glVertex3fv(&ctrlpoints[b][a][0]);
		}
	}
	glEnd();
	glPopMatrix();
}

void Pointer() {
	glPushMatrix();
	glTranslatef(key2.x1, key2.y1, key2.z1);
	glLineWidth(10.0);
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(1000, 0, 0);
	glVertex3f(-1000, 0, 0);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(0, 1000, 0);
	glVertex3f(0, -1000, 0);
	glColor3f(1.0, 1.0, 0.0);
	glVertex3f(0, 0, 1000);
	glVertex3f(0, 0, -1000);
	glEnd();
	glPopMatrix();
}