#include <glut.h> 
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include<Windows.h>
#pragma comment(lib, "winmm.lib") // 소리넣기관련

//가운 데 대관람차
//왼쪽 뒤에 자이로드롭
//왼쪽 앞에 바이킹
//오른쪽 앞에 롤러코스터
//오른쪽 뒤에 회전목마
////////////////////////////////////////////////////////////////////////////////////////////////////
GLvoid drawScene(GLvoid);
GLvoid reshape(int w, int h);
GLubyte* LoadDIBitmap(const char *filename, BITMAPINFO **info);
GLubyte *pBytes; // 데이터를 가리킬 포인터
BITMAPINFO *info; // 비트맵 헤더 저장할 변수
GLuint textures[15];

void SpecialKeyboard(int key, int x, int y);
void Keyboard(unsigned char key, int x, int y);
void Mouse(int button, int state, int x, int y);
void Motion(int x, int y);
void Timer(int value);
void BoyTimer(int value);
void DrawFloor();//바닥 그리기 
void DrawGyroDrop();//자이로 드롭
void DrawViking();//바이킹
void DrawBigWheel();//대관람차
void SquareTower(GLfloat centerx, GLfloat centery, GLfloat centerz, GLfloat radius, GLfloat h);
void BoyPerson(void);//사람
void DrawHorse(void);//말
void DrawMerryGoRound(void);//회전목마
void DrawHotAirBalloon(void);//열기구
void Balloon(void);//열기구
void DrawHouse(void);
void TextureRC(void);
void LightRC(void);


//수정
void Collise(float x1, float x2, float y1, float y2, float tranx, float tranz, int point);
void Collise_BB(float x1, float z1, float x2, float z2, float tranx, float tranz);

bool collise_judge[6] = { false };
void Collise_Timer(int value);
float collise_timer = 0.0;
////////////////////////////////////////////////////////////////////////////////////////////////////
#define GL_PI 3.1415f
float radian = GL_PI / 180.0f;
float viewx = 0.0;//x
float viewy = 0.0;//y
float viewz = 10;//z
float rotate_x = 0.0;
float rotate_y = 0.0;
float rotate_z = 0.0;
float angle = 0;
float anglev = -100;
float angleb = 0;
float titlex = 0;
float titley = 0;

float depth = 20000;

int downx, downy;

bool LeftButtonDown = false;
bool RightButtonDown = false;
bool light1flag = false;
bool light2flag = false;
bool light3flag = false;
bool light4flag = false;
bool light0flag = false;
bool opentitle = false;
bool lightingflag = false;
bool vkflag = false;
GLfloat light_amb1[] = { 0.9, 0.3, 0.3, 1.0 };//주변조명 빨강
GLfloat light_dif1[] = { 0.9, 0.3 ,0.3, 1.0 };//산란조명
GLfloat light_spc1[] = { 1.0, 1.0, 1.0, 1.0 };//거울반사 조명
GLfloat light_pos1[] = { -8000, 0, -8000, 1.0 };//조명의 위치

GLfloat light_amb2[] = { 0.3, 0.9, 0.3, 1.0 };//주변조명 빨강
GLfloat light_dif2[] = { 0.3, 0.9 ,0.3, 1.0 };//산란조명
GLfloat light_spc2[] = { 1.0, 1.0, 1.0, 1.0 };//거울반사 조명
GLfloat light_pos2[] = { -8000, 0, 8000, 1.0 };//조명의 위치

GLfloat light_amb3[] = { 0.3, 0.3, 0.9, 1.0 };//주변조명 빨강
GLfloat light_dif3[] = { 0.3, 0.3 ,0.9, 1.0 };//산란조명
GLfloat light_spc3[] = { 1.0, 1.0, 1.0, 1.0 };//거울반사 조명
GLfloat light_pos3[] = { 8000, 0, 8000, 1.0 };//조명의 위치

GLfloat light_amb4[] = { 0.9, 0.9, 0.3, 1.0 };//주변조명 빨강
GLfloat light_dif4[] = { 0.9, 0.9 ,0.3, 1.0 };//산란조명
GLfloat light_spc4[] = { 1.0, 1.0, 1.0, 1.0 };//거울반사 조명
GLfloat light_pos4[] = { 8000, 0, -8000, 1.0 };//조명의 위치

GLfloat light_amb0[] = { 0.9, 0.5, 0.1, 1.0 };//주변조명 빨강
GLfloat light_dif0[] = { 0.9, 0.5 ,0.1, 1.0 };//산란조명
GLfloat light_spc0[] = { 1.0, 1.0, 1.0, 1.0 };//거울반사 조명
GLfloat light_pos0[] = { 2000, 0, -4000, 1.0 };//조명의 위치

GLUquadricObj *ballon;


GLfloat mat_ambdif[] = { 0.75, 0.75, 0.75, 1.0 };
GLfloat mat_spc[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat mat_shn[] = { 128 };

int ctrol_point_counter = 1;
bool ctrol_point_shape_tf[10] = { 0 };
int ctrol_point_tf = 0;
int ctrol_shape = 1;
int ctrol_shape2 = 0;
int sijum1 = 0;
struct shape {
	double plus = rand() % 10 + 1;
	double scale = 0.05;
	// 회전값
	double rotx = 0;
	double roty = 0;
	double rotz = 0;
	// 이동값
	double tranx = 0;
	double trany = 0;
	double tranz = 0;
	// 이동
	double scalx = 0;
	double scaly = 0;
	double scalz = 0;
	double angle = 0;
	int count = 0;
	int dir = 0;
	int self_dir = 0;
	bool tf = 0;
	bool GD_View = false;
	bool VK_View = false;
	bool MGR_View = false;
	bool BW_View = false;
	bool RC_View = false;
};
struct star {
	float x = 0.0;
	float y = 0.0;
	float z = 0.0;
	float dirx = 0.0;
	float diry = 0.0;
	float dirz = 0.0;
	float colorr = 0.0;
	float colorb = 0.0;
	float colorg = 0.0;
	float movex = 0.0;
	float movey = 0.0;
	float movez = 0.0;
	bool tf = false;
	int dir = 0; // 회전 방향값
	float size = 0.0;
};

shape GyroDrop;//자이로드롭
shape Viking;//바이킹
shape BigWheel;//대관람차
shape person;//사람
shape MerryGoRound;
shape HotAirBallon;
shape person_head;
shape person_lhand_up;
shape person_lhand_down;
shape person_rhand_up;
shape person_rhand_down;
shape person_lleg_up;
shape person_lleg_down;
shape person_rleg_up;
shape person_rleg_down;

//롤러코스터 변수들 *********************************************************************
float timer = 0.0;
float timer1 = 0.2;
float timer2 = 0.4;
float timer3 = 0.6;

float trans_radian = 180 / 3.141592;

float speed1 = 0.01;
float speed2 = 0.01;
float speed3 = 0.01;
float speed4 = 0.01;

int rollercoaster_counter = 0;
int rollercoaster_counter1 = 0;
int rollercoaster_counter2 = 0;
int rollercoaster_counter3 = 0;

shape rollercoaster_main1;
shape rollercoaster_main2;
shape rollercoaster_main3;
shape rollercoaster_main4;

GLfloat ctrlpoints[100][6][3] = { { 0 } }; //베지에 정점 4개 설정
star rollercoster_nose;
star ctrol_point[400];
shape rollercoaster;
void RollerCoaster();
void RollerCoaster_Base();
void RollerCoaster_Set(); //메인 함수에 작성
void RollerCoaster_timer(int value);
void RollerCoaster_Main(); float RollerCoaster_Vecter(float timer, float x, int count, float speed);
float RollerCoaster_Vecter(float timer, float x, int count, float speed);
float RollerCoaster_Inner(float vecter1, float vecter2);

//폭죽폭죽 변수들 *********************************************************************
void FireCracker_First();
void FireCracker_Second(float spot_x, float spot_y, float spot_z);
void FireCracker_Timer(int value);
void FireCracker_Set(); //메인 함수에 작성

shape firecracker_first;
shape firecracker_second;
star firecracker_color;

int firecracker_r = 0;
float firecracker_randi = 3.141592 / 180;

void LightRC(void)
{
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_amb1);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_dif1);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_spc1);
	glLightfv(GL_LIGHT1, GL_POSITION, light_pos1);


	glLightfv(GL_LIGHT2, GL_AMBIENT, light_amb2);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, light_dif2);
	glLightfv(GL_LIGHT2, GL_SPECULAR, light_spc2);
	glLightfv(GL_LIGHT2, GL_POSITION, light_pos2);


	glLightfv(GL_LIGHT3, GL_AMBIENT, light_amb3);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, light_dif3);
	glLightfv(GL_LIGHT3, GL_SPECULAR, light_spc3);
	glLightfv(GL_LIGHT3, GL_POSITION, light_pos3);


	glLightfv(GL_LIGHT4, GL_AMBIENT, light_amb4);
	glLightfv(GL_LIGHT4, GL_DIFFUSE, light_dif4);
	glLightfv(GL_LIGHT4, GL_SPECULAR, light_spc4);
	glLightfv(GL_LIGHT4, GL_POSITION, light_pos4);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_amb0);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_dif0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_spc0);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos0);

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_spc);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shn);

}
void TextureRC(void)
{
	glGenTextures(15, textures);

	glBindTexture(GL_TEXTURE_2D, textures[0]);
	pBytes = LoadDIBitmap("stonetile.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 256, 256, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[1]);
	pBytes = LoadDIBitmap("dark.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 1400, 709, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[2]);
	pBytes = LoadDIBitmap("skydark.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 595, 446, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[3]);
	pBytes = LoadDIBitmap("tile.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 1280, 568, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[4]);
	pBytes = LoadDIBitmap("background_001.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 682, 432, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[5]);
	pBytes = LoadDIBitmap("background_002.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 683, 432, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[6]);
	pBytes = LoadDIBitmap("background_003.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 683, 432, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	////////////////////////////////////////////////
	glBindTexture(GL_TEXTURE_2D, textures[7]);
	pBytes = LoadDIBitmap("leftup.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 640, 384, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[8]);
	pBytes = LoadDIBitmap("rightup.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 640, 384, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[9]);
	pBytes = LoadDIBitmap("leftdown.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 640, 384, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textures[10]);
	pBytes = LoadDIBitmap("rightdown.bmp", &info);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, 640, 384, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, pBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glEnable(GL_DEPTH_TEST);

}

void DrawHotAirBalloon(void)
{
	glPushMatrix();
	{
		glTranslatef(0, 7000, 0);
		{
			glPushMatrix();
			glLineWidth(2);
			glBegin(GL_LINE_STRIP);
			for (int angle = 0; angle < 360; angle++)
			{
				glVertex3f(cos(radian*angle) * 5000, 0, sin(radian*angle) * 5000);

			}
			glEnd();
			glPopMatrix();
		}
		glRotatef(HotAirBallon.roty, 0, 1, 0);
		glTranslatef(0, -100, 0);
		for (int i = 0; i < 360; i += 30)
		{
			glPushMatrix();
			glTranslatef(cos(radian*i) * 5000, 0, sin(radian*i) * 5000);
			Balloon();
			glPopMatrix();
		}
	}
	glPopMatrix();
}


void Balloon(void)
{

	glPushMatrix();
	glScalef(2, 2, 2);
	{
		glPushMatrix();
		glColor3f(1, 0, 1);
		glutSolidSphere(100, 50, 50);
		glColor3f(0, 1, 0);
		glRotatef(90, 1, 0, 0);
		glutSolidTorus(5, 100, 100, 100);//대가리풍선 옆에 줄
		glPopMatrix();
	}
	{
		glPushMatrix();
		glColor3f(0, 1,1);
		glTranslatef(0, -200, 0);
		glRotatef(-90, 1, 0, 0);
		glutSolidCone(3, 25, 30, 30);
		glTranslatef(3, 0, 0);
		glutSolidCone(3, 20, 30, 30);
		glTranslatef(-6, 0, 0);
		glutSolidCone(3, 15, 30, 30);
		glColor3f(1, 0.5, 0);
		glTranslatef(3, -3, 0);
		glutSolidCone(3, 15, 30, 30);
		glTranslatef(0, 6, 0);
		glutSolidCone(3, 15, 30, 30);
		glPopMatrix();
	}
		glPushMatrix();
		glTranslatef(0, -200, 0);
		glColor3f(0.5, 1, 0.5);
		glScalef(0.95, 0.01, 1);
		glutSolidCube(100);
		glPopMatrix();
	
	{
		glColor3f(0, 1, 1);
		glPushMatrix();
		glTranslatef(0, -30, 0);
		glRotatef(90, 1, 0, 0);
		gluCylinder(ballon, 95, 15, 150, 5, 5);//열기구대가리 풍선
		glPopMatrix();
	}
	{
		glPushMatrix();
		glColor3f(0.67, 0.4, 0.1);
		glTranslatef(0, -300, 0);
		glScalef(1, 1, 1);//열기구 아래 바구니
		glutSolidCube(80);
		glPopMatrix();
	}
	{
		glPushMatrix();
		glLineWidth(1);
		glColor3f(1, 1, 0);
		glBegin(GL_LINE_STRIP);
		glVertex3f(70, 0, 80);
		glVertex3f(40, -260, 40);
		glEnd();
		glPopMatrix();
	}
	{
		glPushMatrix();
		glBegin(GL_LINE_STRIP);
		glVertex3f(70, 0, -80);
		glVertex3f(40, -260, -40);
		glEnd();
		glPopMatrix();
	}
	{
		glPushMatrix();
		glBegin(GL_LINE_STRIP);
		glVertex3f(-70, 0, 80);
		glVertex3f(-40, -260, 40);
		glEnd();
		glPopMatrix();
	}
	{
		glPushMatrix();
		glBegin(GL_LINE_STRIP);
		glVertex3f(-70, 0, -80);
		glVertex3f(-40, -260, -40);
		glEnd();
		glPopMatrix();
	}
	glPopMatrix();
}


void DrawHouse(void)
{
	glEnable(GL_BLEND);
	glPushMatrix();
	{
		glTranslatef(3500, 250, 1500);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glColor4f(1, 0, 1, 0.5);
		glPushMatrix();
		glTranslatef(0, 300, 0);
		glScalef(1, 1.7, 3);
		glutSolidTorus(80,300,20,20);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(-350, 350, 0);
		glScalef(1, 4, 1.7);
		glutSolidCube(300);
		glTranslatef(700, 0, 0);
		glutSolidCube(300);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, 800, 0);
		glRotatef(90, 0, 0, 1);
		glScalef(1, 3, 1.7);
		glutSolidCube(300);
		glPopMatrix();

		glPushMatrix();
		glColor4f(1, 1, 0, 0.5);
		glTranslatef(0, 950, 0);
		glRotatef(90, -1 ,0, 0);
		glRotatef(90, 0, 0, 1);
		glScalef(1, 2.5, 1.7);
		glutSolidCone(300,300,30,30);
		glPopMatrix();
	}
	glPopMatrix();
	glDisable(GL_BLEND);
}
GLvoid drawScene()                 // 출력 함수 
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);          // 바탕색을 'blue' 로 지정      
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, -350, -depth);

	if (person_head.tf == true) {
		glTranslatef(0.0, -200.0, 8000);
		glRotatef(person.roty, 0.0, 1.0, 0.0);
		glTranslatef(-person.tranx, 0.0, -person.tranz);
	}
	else if (collise_judge[3] == true)//회전목마
	{
		glTranslatef(0.0, -200.0, 8000);//중점으로 옮기고
		gluLookAt(320 * cos(angle*radian) + 2000, -400 + MerryGoRound.trany, 320 * -sin(angle*radian) - 2000,
			320 * cos((angle + 1) * radian) + 2000, -400 + MerryGoRound.trany, 320 * -sin((angle + 1)*radian) - 2000,
			0, 1, 0);
	}
	else if (collise_judge[0] == true)//바이킹
	{
		glTranslatef(0.0, -200.0, 8000);//중점으로 옮기고
		gluLookAt(-2000, 1000 * sin(anglev*radian) +1000, 1000 * -cos(anglev* radian) + 2000,
			-2000, 1000 * sin((anglev + 1)*radian) + 1000, 1000 * -cos((anglev + 1) * radian) + 2000,
			0, 1, 0);
	}
	else if (collise_judge[1] == true)//자이로드롭
	{
		glTranslatef(0.0, -200.0, 8000);//중점으로 옮기고
		gluLookAt(75 * cos(GyroDrop.roty*radian) - 2000, 100 + GyroDrop.tranz*2.5, 75 * -sin(GyroDrop.roty*radian) - 2000,
			75 * cos((GyroDrop.roty + 1) * radian) - 2000, 100 + GyroDrop.tranz*2.5, 75 * -sin((GyroDrop.roty + 1)*radian) - 2000,
			0, 1, 0);
	}
	else if (collise_judge[2] == true)//대관람차
	{
		glTranslatef(0.0, -200.0, 6850);//중점으로 옮기고
		gluLookAt(1450 * cos(BigWheel.rotz* radian), 1450 * sin(BigWheel.rotz*radian) + 1200, 0,
			1450 * cos((BigWheel.rotz) * radian) + 1, 1450 * sin((BigWheel.rotz)*radian) + 1200, -1,
			0, 1, 0);

	}
	//롤러 코스터 시점 변환 수정
	else if (collise_judge[4] == true) {
		glTranslatef(0, -350.0, depth);//중점으로 옮기고
		glRotated(-rollercoaster_main4.roty - 90, 0, 1, 0);
		glTranslatef(-rollercoaster_main4.tranx, -rollercoaster_main4.trany, -rollercoaster_main4.tranz);
	}
	else {
		glTranslatef(0, 0, viewz);
		glRotatef(viewx, 0, 1, 0);
		glRotatef(viewy, -1, 0, 0);

		glRotatef(rotate_x, 1.0, 0.0, 0.0);
		glRotatef(rotate_y, 0.0, 1.0, 0.0);
		glRotatef(rotate_z, 0.0, 0.0, 1.0);
	}
	//충돌 박스// x1, y1, x2, y2 tranx, trany //수정
	Collise_BB(-500, -1100, 500, 1100, -2000, 2000); //바이킹 출돌박스
	Collise_BB(-300, -300, 300, 300, -2000, -2000); //자이로드롭 출돌박스
	Collise_BB(-600, -600, 600, 600, 0, 1000); //관람열차 출돌박스
	Collise_BB(-500, -500, 500, 500, 2000, -2000); //회전목마 출돌박스
	Collise_BB(-500, -300, 500, 300, 3500, 1500); //롤러코스터

	Collise(-500, -1100, 500, 1100, -2000, 2000, 0);
	Collise(-300, -300, 300, 300, -2000, -2000, 1);
	Collise(-600, -600, 600, 600, 0, 1000, 2);
	Collise(-500, -500, 500, 500, 2000, -2000, 3);
	Collise(-500, -300, 500, 300, 3500, 1500, 4);

	DrawFloor();
	DrawHouse();
	DrawHotAirBalloon();
	DrawMerryGoRound();
	
	DrawGyroDrop();
	DrawViking();
	DrawBigWheel();
	BoyPerson();
	glPushMatrix();
	glTranslatef(-400, 100, 0);
	RollerCoaster();
	RollerCoaster_Base();
	FireCracker_First();

	glPopMatrix();
	glutSwapBuffers();
}
star ctrol_point1[400];
////////////////////////////////////////////////////////////////////////////////////////////////////
GLvoid SpecialKeyboard(int key, int x, int y)
{
	if (key == GLUT_KEY_LEFT) {
		if (person_head.tf == false) {
			person.roty = 270;
			person.tranx -= 15;
			person.tf = true;
		}
		else {
			if (person.roty == 0) {
				person.roty = 90;
			}
			else if (person.roty == 90) {
				person.roty = 180;
			}
			else if (person.roty == 180) {
				person.roty = 270;
			}
			else if (person.roty == 270) {
				person.roty = 0;
			}
		}
	}
	if (key == GLUT_KEY_RIGHT) {
		if (person_head.tf == false) {
			person.roty = 90;
			person.tranx += 15;
			person.tf = true;
		}
		else {
			if (person.roty == 0) {
				person.roty = 270;
			}
			else if (person.roty == 90) {
				person.roty = 0;
			}
			else if (person.roty == 180) {
				person.roty = 90;
			}
			else if (person.roty == 270) {
				person.roty = 180;
			}
		}
	}
	if (key == GLUT_KEY_UP) {
		if (person_head.tf == false) {
			person.roty = 180;
			person.tranz -= 15;
			person.tf = true;
			printf("^");
		}
		else {
			if (person.roty == 0) {
				person.tranz -= 15;
			}
			else if (person.roty == 90) {
				person.tranx += 15;
			}
			else if (person.roty == 180) {
				person.tranz += 15;
			}
			else if (person.roty == 270) {
				person.tranx -= 15;
			}
		}
	}
	if (key == GLUT_KEY_DOWN) {
		if (person_head.tf == false) {
			person.roty = 0;
			person.tranz += 15;

			person.tf = true;
		}
		else {
			if (person.roty == 0) {
				person.tranz += 115;
			}
			else if (person.roty == 90) {
				person.tranx -= 15;
			}
			else if (person.roty == 180) {
				person.tranz -= 15;
			}
			else if (person.roty == 270) {
				person.tranx += 15;
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////
int moving_count = 0;
void BoyTimer(int value)
{
	//person_head.rotz += 2;
	if (person.tf == true) {
		if (person_lleg_up.dir == 0) {
			person_lleg_up.rotz -= 5;
			person_lleg_down.rotz += 5;
			person_rleg_up.rotz += 3;
			//손 흔들기
			person_lhand_up.rotz += 5;
			person_lhand_down.rotz -= 3;
			person_rhand_up.rotz -= 5;
			person_rhand_down.rotz -= 3;
			// 머리통
			person_head.rotz += 1;
			if (person_lleg_down.rotz > 40) {
				person_lleg_up.dir = 1;
				person.tf = false;
			}
		}
		if (person_lleg_up.dir == 1) {
			person_lleg_up.rotz += 5;
			person_lleg_down.rotz -= 5;
			person_rleg_up.rotz -= 3;
			person_lhand_up.rotz -= 5;
			person_lhand_down.rotz += 3;
			person_rhand_up.rotz += 5;
			person_rhand_down.rotz += 3;
			person_head.rotz -= 1;
			if (person_lleg_down.rotz < 0) {
				person_lleg_up.dir = 2;
			}
		}
		if (person_lleg_up.dir == 2) {
			person_rleg_up.rotz -= 5;
			person_rleg_down.rotz += 5;
			person_lleg_up.rotz += 3;
			person_lhand_up.rotz -= 5;
			person_lhand_down.rotz -= 3;
			person_rhand_up.rotz += 5;
			person_rhand_down.rotz -= 3;
			person_head.rotz += 1;
			if (person_rleg_down.rotz > 40) {
				person_lleg_up.dir = 3;
			}
		}
		if (person_lleg_up.dir == 3) {
			person_rleg_up.rotz += 5;
			person_rleg_down.rotz -= 5;
			person_lleg_up.rotz -= 3;
			person_lhand_up.rotz += 5;
			person_lhand_down.rotz += 3;
			person_rhand_up.rotz -= 5;
			person_rhand_down.rotz += 3;
			person_head.rotz -= 1;
			if (person_rleg_down.rotz < 0) {
				person_lleg_up.dir = 0;
				person.tf = false;
			}
		}
	}
	glutPostRedisplay();
	glutTimerFunc(40, BoyTimer, 1);
}


void Timer(int value)
{
	HotAirBallon.roty += 0.5;

	MerryGoRound.roty += 1;
	if (MerryGoRound.tf == false)
	{
		if (MerryGoRound.trany < 70)
		{
			MerryGoRound.tranz += 2;
			MerryGoRound.trany += 2;
		}
		else
			MerryGoRound.tf = true;
	}
	else if (MerryGoRound.tf == true)
	{
		if (MerryGoRound.trany > 0)
		{
			MerryGoRound.tranz -= 2;
			MerryGoRound.trany -= 2;
		}
		else
			MerryGoRound.tf = false;
	}
	angle += 1;
	if (opentitle == true)
	{
		if (titlex < 4000)
		{
			titlex += 25;
			titley += 25;
		}
		else
		{
			if (depth > 8000)
			{
				depth -= 100;
			}
		}
	}
	else if (opentitle == false)
	{

		if (depth < 20000)
		{
			depth += 100;
		}
		else
		{
			if (titlex > 0)
			{
				titlex -= 25;
				titley -= 25;
			}
		}
	}
	

	BigWheel.rotz += 0.5;
	if (BigWheel.rotz > 300) {
		BigWheel.rotz =0;
		collise_judge[2] = false;
	}

	if (GyroDrop.tf == false)
	{
		if (GyroDrop.tranz < 900)
		{
			GyroDrop.roty += 0.5;
			GyroDrop.tranz += 0.5;
		}
		else
			GyroDrop.tf = true;
	}
	else if (GyroDrop.tf == true)
	{
		GyroDrop.roty += 0;
		if (GyroDrop.tranz > 0)
		{
			GyroDrop.tranz -= 1;
			if (GyroDrop.tranz >= 50)
			{
				GyroDrop.tranz -= 30;
			}
		}
		else 
		{
			GyroDrop.tf = false;
			collise_judge[1] = false; // 수정
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
	if (Viking.tf == false)
	{
		if (Viking.rotx < 80)
		{
			Viking.rotx += 2;
		}
		else
			Viking.tf = true;
	}
	else if (Viking.tf == true)
	{
		if (Viking.rotx > -80)
		{
			Viking.rotx -= 2;
		}
		else
			Viking.tf = false;
	}

	////////////////////
	if (vkflag == false)
	{
		if (anglev < -20)
		{
			anglev += 2;
		}
		else
			vkflag = true;
	}
	else if (vkflag == true)
	{
		if (anglev > -180)
		{
			anglev -= 2;
		}
		else
			vkflag = false;
	}


	glutPostRedisplay();
	glutTimerFunc(20, Timer, 1);
}
////////////////////////////////////////////////////////////////////////////////////////////////////
void Keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case '1': {
		rotate_x += 1;
		break;
	}
	case '2': {
		rotate_y += 1;
		break;
	}
	case '3': {
		rotate_z += 1;
		break;
	}
	case'i': {
		rotate_x = 0;
		rotate_y = 0;
		rotate_z = 0;
		break;
	}
	case '5': {
		person.count++;
		if (person.count % 2 == 1) {
			person_head.tf = true;
		}
		else {
			person_head.tf = false;
		}
	}
	case 'q':
	{
		exit(0);
	}
	}
	if (key == '6')
	{
		if (light1flag == false) {
			glEnable(GL_LIGHT1);
			light1flag = true;
		}
		else
		{
			glDisable(GL_LIGHT1);
			light1flag = false;
		}
	}
	if (key == '7')
	{
		if (light2flag == false) {
			glEnable(GL_LIGHT2);
			light2flag = true;
		}
		else
		{
			glDisable(GL_LIGHT2);
			light2flag = false;
		}
	}
	if (key == '8')
	{
		if (light3flag == false) {
			glEnable(GL_LIGHT3);
			light3flag = true;
		}
		else
		{
			glDisable(GL_LIGHT3);
			light3flag = false;
		}
	}
	if (key == '9')
	{
		if (light4flag == false) {
			glEnable(GL_LIGHT4);
			light4flag = true;
		}
		else
		{
			glDisable(GL_LIGHT4);
			light4flag = false;
		}
	}
	else if (key == '0')//전체조명
	{
		{
			if (lightingflag == false) {
				lightingflag = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				glDisable(GL_LIGHTING);
				lightingflag = false;
			}
		}
	}
	else if (key == '`')//전체조명
	{
		{
			if (light0flag == false) {
				light0flag = true;
				glEnable(GL_LIGHT0);
			}
			else
			{
				glDisable(GL_LIGHT0);
				light0flag = false;
			}
		}
	}
	else if (key == ' ')
	{
		{
			if (opentitle == false) {
				opentitle = true;
			}
			else
			{
				opentitle = false;
			}
		}
	}

	glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
void main(int argc, char *argv[]) {
	PlaySound("bgm.wav", NULL, SND_FILENAME | SND_ASYNC | SND_LOOP | SND_NODEFAULT);
	ballon = gluNewQuadric();
	srand((unsigned)time(NULL));
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(200, 70);
	glutInitWindowSize(800, 600);
	FireCracker_Set();//폭죽 초기화
	RollerCoaster_Set();
	glutCreateWindow("Points Drawing");
	TextureRC();
	LightRC();
	glutKeyboardFunc(Keyboard);
	glutSpecialFunc(SpecialKeyboard);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);  
	glutTimerFunc(100, FireCracker_Timer, 1);
	glutTimerFunc(100, RollerCoaster_timer, 1);
	glutTimerFunc(20, Timer, 1);
	glutTimerFunc(40, BoyTimer, 1);
	glutTimerFunc(40, Collise_Timer, 1); //수정
	glutDisplayFunc(drawScene);
	glutReshapeFunc(reshape);
	glutMainLoop();
}

//롤러코스터 좌표 설정
void RollerCoaster_Set() {
	FILE *f;
	f = fopen("map.txt", "r");
	for (int i = 0; i < 100; i++) {
		for (int j = 0; j < 6; j++) {
			fscanf(f, "%f %f %f", &ctrlpoints[i][j][0], &ctrlpoints[i][j][1], &ctrlpoints[i][j][2]);
		}
	}
	fclose(f);
	for (int i = 0; i < 100; i++) {
		for (int j = 0; j < 5; j++) {
			printf("%f %f %f \n", ctrlpoints[i][j][0], ctrlpoints[i][j][1], ctrlpoints[i][j][2]);
		}
	}
}
//////롤러코스터///////////////////////////////
void RollerCoaster_Base() {
	glPushMatrix();
	glPointSize(20.0);
	glLineWidth(2.0);
	/////////////////////////////// 베지에 곡선
	for (int ctrl = 0; ctrl < 100; ctrl++) {
		glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 6, &ctrlpoints[ctrl][0][0]);
		glEnable(GL_MAP1_VERTEX_3);
		// 선그리기
		glColor3f(1, 0.7, 0);
		glBegin(GL_LINE_STRIP);
		for (int line = 0; line < 500; line++) {
			glEvalCoord1f((GLfloat)line / 500);
		}
		glEnd();
		glDisable(GL_MAP1_VERTEX_3);
	}
	glPopMatrix();
	glPushMatrix();

	glScalef(1.01, 1.01, 1.0);
	glPointSize(20.0);
	glLineWidth(2.0);
	/////////////////////////////// 베지에 곡선
	for (int ctrl = 0; ctrl < 100; ctrl++) {
		glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 6, &ctrlpoints[ctrl][0][0]);
		glEnable(GL_MAP1_VERTEX_3);
		// 선그리기
		glBegin(GL_LINE_STRIP);
		for (int line = 0; line < 500; line++) {
			glEvalCoord1f((GLfloat)line / 500);
		}
		glEnd();
		glDisable(GL_MAP1_VERTEX_3);
	}

	glPopMatrix();
}

// 열차 본체
void RollerCoaster_Main()
{
	GLUquadricObj *leg;
	leg = gluNewQuadric();
	{
		glPushMatrix();
		glColor3f(0.57, 0.3, 0.2);
		glScalef(1.5, 1.0, 1.0);
		glutSolidCube(230);//몸통
		glPopMatrix();//몸통
	}
	/////////////////////////////////다리///////////////////////////////////
	{
		glPushMatrix();
		glColor3f(0.57, 0.3, 0.1);
		glTranslatef(-130, -100, 150);
		glScalef(0.5, 2, 0.5);
		glutSolidSphere(50, 30, 30);
		glPopMatrix();
	}
	{
		glPushMatrix();
		glTranslatef(-130, -100, -150);
		glScalef(0.5, 2, 0.5);
		glutSolidSphere(50, 30, 30);//몸통
		glPopMatrix();//몸통
	} {
		glPushMatrix();
		glTranslatef(130, -100, 150);
		glScalef(0.5, 2, 0.5);
		glutSolidSphere(50, 30, 30);//몸통
		glPopMatrix();//몸통
	} {
		glPushMatrix();
		glTranslatef(130, -100, -150);
		glScalef(0.5, 2, 0.5);
		glutSolidSphere(50, 30, 30);//몸통
		glPopMatrix();//몸통
	}
	/////////////////////////////굽/////////////////////////////////
	{
		glPushMatrix();
		glColor3f(0.2, 0.2, 0.2);
		glTranslatef(-130, -200, 150);
		glRotatef(-90, 1, 0, 0);
		glScalef(0.8, 1.0, 0.8);

		gluCylinder(leg, 40, 20, 150, 100, 100);//몸통
		glPopMatrix();//몸통
	}
	{
		glPushMatrix();
		glTranslatef(-130, -200, -150);
		glRotatef(-90, 1, 0, 0);
		glScalef(0.8, 1.0, 0.8);

		gluCylinder(leg, 40, 20, 150, 100, 100);//몸통
		glPopMatrix();//몸통
	} {
		glPushMatrix();
		glTranslatef(130, -200, 150);
		glRotatef(-90, 1, 0, 0);
		glScalef(0.8, 1.0, 0.8);

		gluCylinder(leg, 40, 20, 150, 100, 100);//몸통
		glPopMatrix();//몸통
	} {
		glPushMatrix();
		glTranslatef(130, -200, -150);
		glRotatef(-90, 1, 0, 0);
		glScalef(0.8, 1.0, 0.8);

		gluCylinder(leg, 40, 20, 150, 100, 100);//몸통
		glPopMatrix();//몸통
	}
	//////////////////////////////////////////////////////////////////

	{
		glPushMatrix();
		glTranslatef(220, 20, 0);
		glColor3f(1, 1, 1);
		glutSolidSphere(40, 30, 30);
		glPopMatrix();//꼬리
	}

	{
		glPushMatrix();
		glColor3f(0.57, 0.3, 0.1);
		glTranslatef(-235, 30, 0);
		glScalef(1.0, 0.9, 0.9);
		glutSolidSphere(150, 50, 50);
		glPopMatrix();//얼굴
	}
	{
		glPushMatrix();
		glColor3f(1, 0, 0);
		glTranslatef(-185, 25, 0);
		glRotatef(90, 0.3, 1, 0);
		glScalef(1, 1, 1.5);
		glutSolidTorus(15, 140, 50, 50);
		glPopMatrix();//목도리
	}
	{
		glPushMatrix();
		glColor3f(1, 1, 0);
		glTranslatef(-225, -120, 0);
		glutSolidSphere(40, 30, 30);
		glPopMatrix();//종
	}

	//////////////////////////////왼쪽뿔/////////////////////////
	{
		glPushMatrix();//뿔
		glLineWidth(1.0);
		glColor3f(0.57, 0.3, 0.3);
		glTranslatef(-230, 150, 50);
		glRotatef(-70, 1, 0, 0);
		glScalef(0.4, 0.4, 2.0);
		SquareTower(0, 0, 0, 50, 100);
		glPopMatrix();

	}
	{
		glPushMatrix();//뿔
		glLineWidth(1.0);
		glColor3f(0.57, 0.3, 0.3);
		glTranslatef(-230, 260, 80);
		glScalef(0.4, 0.4, 1.0);
		SquareTower(0, 0, 0, 40, 100);
		glPopMatrix();

	}
	{
		glPushMatrix();//뿔
		glLineWidth(1.0);
		glColor3f(0.57, 0.3, 0.3);
		glTranslatef(-230, 330, 20);
		glRotatef(30, 1, 0, 0);
		glScalef(0.4, 0.4, 0.8);
		SquareTower(0, 0, 0, 40, 100);
		glPopMatrix();

	}
	///////////////////////////////오른쪽뿔/////////////////////////////
	{
		glPushMatrix();//뿔
		glLineWidth(1.0);
		glColor3f(0.57, 0.3, 0.3);
		glTranslatef(-230, 150, -50);
		glRotatef(-110, 1, 0, 0);
		glScalef(0.4, 0.4, 2.0);
		SquareTower(0, 0, 0, 50, 100);

		glPopMatrix();
	}
	{
		glPushMatrix();//뿔
		glLineWidth(1.0);
		glColor3f(0.57, 0.3, 0.3);
		glTranslatef(-230, 260, -180);

		glScalef(0.4, 0.4, 1.0);
		SquareTower(0, 0, 0, 40, 100);

		glPopMatrix();

	}
	{
		glPushMatrix();//뿔
		glLineWidth(1.0);
		glColor3f(0.57, 0.3, 0.3);
		glTranslatef(-230, 290, -110);
		glRotatef(-30, 1, 0, 0);
		glScalef(0.4, 0.4, 1.0);
		SquareTower(0, 0, 0, 40, 100);

		glPopMatrix();

	}
	////////////////////////////////////////////////////////////////
	{
		glPushMatrix();//코
		glColor3f(rollercoster_nose.colorr, rollercoster_nose.colorb, rollercoster_nose.colorg);
		//glColor3f(1, 0, 0);
		glTranslatef(-420, 30, 0);
		glutSolidSphere(40, 30, 30);
		glPopMatrix();
	}
}

void RollerCoaster() {
	glPushMatrix();
	glTranslatef(rollercoaster_main1.tranx, rollercoaster_main1.trany, rollercoaster_main1.tranz);
	glRotatef(rollercoaster_main1.roty, 0, 1, 0);
	RollerCoaster_Main();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(rollercoaster_main2.tranx, rollercoaster_main2.trany, rollercoaster_main2.tranz);
	glRotatef(rollercoaster_main2.roty, 0, 1, 0);
	RollerCoaster_Main();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(rollercoaster_main3.tranx, rollercoaster_main3.trany, rollercoaster_main3.tranz);
	glRotatef(rollercoaster_main3.roty, 0, 1, 0);
	RollerCoaster_Main();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(rollercoaster_main4.tranx, rollercoaster_main4.trany, rollercoaster_main4.tranz);
	glRotatef(rollercoaster_main4.roty, 0, 1, 0);
	RollerCoaster_Main();
	glPopMatrix();
}

void RollerCoaster_timer(int value) {
	timer += speed1;
	timer1 += speed1;
	timer2 += speed1;
	timer3 += speed1;

	float a1 = (1.0 - timer) * (1.0 - timer) * (1.0 - timer) * (1.0 - timer)* (1.0 - timer);
	float a2 = 5 * timer * (1.0 - timer) * (1.0 - timer) * (1.0 - timer)* (1.0 - timer);
	float a3 = 10 * timer * timer * (1.0 - timer) * (1.0 - timer)* (1.0 - timer);
	float a4 = 10 * timer * timer * timer * (1.0 - timer) * (1.0 - timer);
	float a5 = 5 * timer * timer  * timer * timer * (1.0 - timer);
	float a6 = timer * timer * timer * timer * timer;

	float b1 = (1.0 - timer1) * (1.0 - timer1) * (1.0 - timer1) * (1.0 - timer1)* (1.0 - timer1);
	float b2 = 5 * timer1 * (1.0 - timer1) * (1.0 - timer1) * (1.0 - timer1)* (1.0 - timer1);
	float b3 = 10 * timer1 * timer1 * (1.0 - timer1) * (1.0 - timer1)* (1.0 - timer1);
	float b4 = 10 * timer1 * timer1 * timer1 * (1.0 - timer1) * (1.0 - timer1);
	float b5 = 5 * timer1 * timer1  * timer1 * timer1 * (1.0 - timer1);
	float b6 = timer1 * timer1 * timer1 * timer1 * timer1;

	float c1 = (1.0 - timer2) * (1.0 - timer2) * (1.0 - timer2) * (1.0 - timer2)* (1.0 - timer2);
	float c2 = 5 * timer2 * (1.0 - timer2) * (1.0 - timer2) * (1.0 - timer2)* (1.0 - timer2);
	float c3 = 10 * timer2 * timer2 * (1.0 - timer2) * (1.0 - timer2)* (1.0 - timer2);
	float c4 = 10 * timer2 * timer2 * timer2 * (1.0 - timer2) * (1.0 - timer2);
	float c5 = 5 * timer2 * timer2  * timer2 * timer2 * (1.0 - timer2);
	float c6 = timer2 * timer2 * timer2 * timer2 * timer2;

	float d1 = (1.0 - timer3) * (1.0 - timer3) * (1.0 - timer3) * (1.0 - timer3)* (1.0 - timer3);
	float d2 = 5 * timer3 * (1.0 - timer3) * (1.0 - timer3) * (1.0 - timer3)* (1.0 - timer3);
	float d3 = 10 * timer3 * timer3 * (1.0 - timer3) * (1.0 - timer3)* (1.0 - timer3);
	float d4 = 10 * timer3 * timer3 * timer3 * (1.0 - timer3) * (1.0 - timer3);
	float d5 = 5 * timer3 * timer3  * timer3 * timer3 * (1.0 - timer3);
	float d6 = timer3 * timer3 * timer3 * timer3 * timer3;

	rollercoaster_main1.tranx = a1 *  ctrlpoints[rollercoaster_counter][0][0] + a2 *  ctrlpoints[rollercoaster_counter][1][0] + a3 *  ctrlpoints[rollercoaster_counter][2][0] + a4 *  ctrlpoints[rollercoaster_counter][3][0] + a5 * ctrlpoints[rollercoaster_counter][4][0] + a6 * ctrlpoints[rollercoaster_counter][5][0];
	rollercoaster_main1.trany = a1 *  ctrlpoints[rollercoaster_counter][0][1] + a2 *  ctrlpoints[rollercoaster_counter][1][1] + a3 *  ctrlpoints[rollercoaster_counter][2][1] + a4 *  ctrlpoints[rollercoaster_counter][3][1] + a5 * ctrlpoints[rollercoaster_counter][4][1] + a6 * ctrlpoints[rollercoaster_counter][5][1];
	rollercoaster_main1.tranz = a1 *  ctrlpoints[rollercoaster_counter][0][2] + a2 *  ctrlpoints[rollercoaster_counter][1][2] + a3 *  ctrlpoints[rollercoaster_counter][2][2] + a4 *  ctrlpoints[rollercoaster_counter][3][2] + a5 * ctrlpoints[rollercoaster_counter][4][2] + a6 * ctrlpoints[rollercoaster_counter][5][2];

	rollercoaster_main2.tranx = b1 *  ctrlpoints[rollercoaster_counter1][0][0] + b2 *  ctrlpoints[rollercoaster_counter1][1][0] + b3 *  ctrlpoints[rollercoaster_counter1][2][0] + b4 *  ctrlpoints[rollercoaster_counter1][3][0] + b5 * ctrlpoints[rollercoaster_counter1][4][0] + b6 * ctrlpoints[rollercoaster_counter1][5][0];
	rollercoaster_main2.trany = b1 *  ctrlpoints[rollercoaster_counter1][0][1] + b2 *  ctrlpoints[rollercoaster_counter1][1][1] + b3 *  ctrlpoints[rollercoaster_counter1][2][1] + b4 *  ctrlpoints[rollercoaster_counter1][3][1] + b5 * ctrlpoints[rollercoaster_counter1][4][1] + b6 * ctrlpoints[rollercoaster_counter1][5][1];
	rollercoaster_main2.tranz = b1 *  ctrlpoints[rollercoaster_counter1][0][2] + b2 *  ctrlpoints[rollercoaster_counter1][1][2] + b3 *  ctrlpoints[rollercoaster_counter1][2][2] + b4 *  ctrlpoints[rollercoaster_counter1][3][2] + b5 * ctrlpoints[rollercoaster_counter1][4][2] + b6 * ctrlpoints[rollercoaster_counter1][5][2];

	rollercoaster_main3.tranx = c1 *  ctrlpoints[rollercoaster_counter2][0][0] + c2 *  ctrlpoints[rollercoaster_counter2][1][0] + c3 *  ctrlpoints[rollercoaster_counter2][2][0] + c4 *  ctrlpoints[rollercoaster_counter2][3][0] + c5 * ctrlpoints[rollercoaster_counter2][4][0] + c6 * ctrlpoints[rollercoaster_counter2][5][0];
	rollercoaster_main3.trany = c1 *  ctrlpoints[rollercoaster_counter2][0][1] + c2 *  ctrlpoints[rollercoaster_counter2][1][1] + c3 *  ctrlpoints[rollercoaster_counter2][2][1] + c4 *  ctrlpoints[rollercoaster_counter2][3][1] + c5 * ctrlpoints[rollercoaster_counter2][4][1] + c6 * ctrlpoints[rollercoaster_counter2][5][1];
	rollercoaster_main3.tranz = c1 *  ctrlpoints[rollercoaster_counter2][0][2] + c2 *  ctrlpoints[rollercoaster_counter2][1][2] + c3 *  ctrlpoints[rollercoaster_counter2][2][2] + c4 *  ctrlpoints[rollercoaster_counter2][3][2] + c5 * ctrlpoints[rollercoaster_counter2][4][2] + c6 * ctrlpoints[rollercoaster_counter2][5][2];

	rollercoaster_main4.tranx = d1 *  ctrlpoints[rollercoaster_counter3][0][0] + d2 *  ctrlpoints[rollercoaster_counter3][1][0] + d3 *  ctrlpoints[rollercoaster_counter3][2][0] + d4 *  ctrlpoints[rollercoaster_counter3][3][0] + d5 * ctrlpoints[rollercoaster_counter3][4][0] + d6 * ctrlpoints[rollercoaster_counter3][5][0];
	rollercoaster_main4.trany = d1 *  ctrlpoints[rollercoaster_counter3][0][1] + d2 *  ctrlpoints[rollercoaster_counter3][1][1] + d3 *  ctrlpoints[rollercoaster_counter3][2][1] + d4 *  ctrlpoints[rollercoaster_counter3][3][1] + d5 * ctrlpoints[rollercoaster_counter3][4][1] + d6 * ctrlpoints[rollercoaster_counter3][5][1];
	rollercoaster_main4.tranz = d1 *  ctrlpoints[rollercoaster_counter3][0][2] + d2 *  ctrlpoints[rollercoaster_counter3][1][2] + d3 *  ctrlpoints[rollercoaster_counter3][2][2] + d4 *  ctrlpoints[rollercoaster_counter3][3][2] + d5 * ctrlpoints[rollercoaster_counter3][4][2] + d6 * ctrlpoints[rollercoaster_counter3][5][2];
	/////////////////////////////////////////////////////////////////////////////////////////////////

	// 첫 번째 차 벡터
	float main1_vecterx = RollerCoaster_Vecter(timer, rollercoaster_main1.tranx, 0, 1);
	float main1_vectery = RollerCoaster_Vecter(timer, rollercoaster_main1.trany, 1, 1);
	float main1_vecterz = RollerCoaster_Vecter(timer, rollercoaster_main1.tranz, 2, 1);

	// 두번째 번째 차 벡터
	float main2_vecterx = RollerCoaster_Vecter(timer1, rollercoaster_main2.tranx, 0, 2);
	float main2_vectery = RollerCoaster_Vecter(timer1, rollercoaster_main2.trany, 1, 2);
	float main2_vecterz = RollerCoaster_Vecter(timer1, rollercoaster_main2.tranz, 2, 2);

	// 세번째 번째 차 벡터
	float main3_vecterx = RollerCoaster_Vecter(timer2, rollercoaster_main3.tranx, 0, 3);
	float main3_vectery = RollerCoaster_Vecter(timer2, rollercoaster_main3.trany, 1, 3);
	float main3_vecterz = RollerCoaster_Vecter(timer2, rollercoaster_main3.tranz, 2, 3);

	// 네번째 번째 차 벡터
	float main4_vecterx = RollerCoaster_Vecter(timer3, rollercoaster_main4.tranx, 0, 4);
	float main4_vectery = RollerCoaster_Vecter(timer3, rollercoaster_main4.trany, 1, 4);
	float main4_vecterz = RollerCoaster_Vecter(timer3, rollercoaster_main4.tranz, 2, 4);

	rollercoaster_main1.roty = RollerCoaster_Inner(main1_vecterx, main1_vecterz);
	rollercoaster_main2.roty = RollerCoaster_Inner(main2_vecterx, main2_vecterz);
	rollercoaster_main3.roty = RollerCoaster_Inner(main3_vecterx, main3_vecterz);
	rollercoaster_main4.roty = RollerCoaster_Inner(main4_vecterx, main4_vecterz);

	////////////////////////////////////////////////////////////////////////////////////////////////
	if (timer > 1) { timer = 0; rollercoaster_counter++; }
	if (timer1 > 1) { timer1 = 0; rollercoaster_counter1++; }
	if (timer2 > 1) { timer2 = 0; rollercoaster_counter2++; }
	if (timer3 > 1) { timer3 = 0; rollercoaster_counter3++; }

	if (ctrlpoints[rollercoaster_counter][0][0] == 0 && ctrlpoints[rollercoaster_counter][0][1] == 0 && ctrlpoints[rollercoaster_counter][0][2] == 0) {
		rollercoaster_counter = 0;
	}
	if (ctrlpoints[rollercoaster_counter1][0][0] == 0 && ctrlpoints[rollercoaster_counter1][0][1] == 0 && ctrlpoints[rollercoaster_counter1][0][2] == 0) {
		rollercoaster_counter1 = 0;
	}
	if (ctrlpoints[rollercoaster_counter2][0][0] == 0 && ctrlpoints[rollercoaster_counter2][0][1] == 0 && ctrlpoints[rollercoaster_counter2][0][2] == 0) {
		rollercoaster_counter2 = 0;
	}
	if (ctrlpoints[rollercoaster_counter3][0][0] == 0 && ctrlpoints[rollercoaster_counter3][0][1] == 0 && ctrlpoints[rollercoaster_counter3][0][2] == 0) {
		rollercoaster_counter3 = 0;
		collise_judge[4] = false;
	}

	// 코 색깔 변화
	rollercoster_nose.colorr = (rand() % 10) * 0.1;
	rollercoster_nose.colorb = (rand() % 10) * 0.1;
	rollercoster_nose.colorg = (rand() % 10) * 0.1;

	glutTimerFunc(30, RollerCoaster_timer, 1);
	glutPostRedisplay();
}
// 내적 x, z 축
float RollerCoaster_Inner(float vecter1, float vecter2) { // (-1, 0, 0) , 벡터 1
	float v1 = -1 * vecter1; // -1 * vecterx + 0 * vectery
	float v2 = sqrt((vecter1 * vecter1) + (vecter2 * vecter2));
	float rotate_xz = acos(v1 / v2) * trans_radian;
	float outer = -1 * vecter2; // 외적 값이 0보다 크다면

	if (outer < 0) {
		printf("1 : ");
		printf("%f \n", rotate_xz);
		return  rotate_xz;
	}
	else if (outer = 0) {
		printf("2 : ");
	}
	else {
		printf("3 : ");
		printf("%f \n", rotate_xz);
		return  -rotate_xz;
	}
}
float RollerCoaster_Vecter(float timer, float x, int count, float speed) {
	float times = timer - 0.01;
	float aa1 = (1.0 - times) * (1.0 - times) * (1.0 - times) * (1.0 - times)* (1.0 - times);
	float aa2 = 5 * times * (1.0 - times) * (1.0 - times) * (1.0 - times)* (1.0 - times);
	float aa3 = 10 * times * times * (1.0 - times) * (1.0 - times)* (1.0 - times);
	float aa4 = 10 * times * times * times * (1.0 - times) * (1.0 - times);
	float aa5 = 5 * times * times  * times * times * (1.0 - times);
	float aa6 = times * times * times * times * times;
	float tranx1 = x;
	float tranx = aa1 *  ctrlpoints[rollercoaster_counter][0][count] + aa2 *  ctrlpoints[rollercoaster_counter][1][count] + aa3 *  ctrlpoints[rollercoaster_counter][2][count] + aa4 *  ctrlpoints[rollercoaster_counter][3][count] + aa5 * ctrlpoints[rollercoaster_counter][4][count] + aa6 * ctrlpoints[rollercoaster_counter][5][count];
	float vecter = tranx1 - tranx;
	if (count == 1) {//y값을 비교하여 속력 변화를 줌   
		if (x < tranx) {
			if (speed == 1) {
				speed1 += 0.001;
			}
			if (speed == 2) {
				speed2 += 0.001;
			}
			if (speed == 3) {
				speed3 += 0.001;
			}
			if (speed == 4) {
				speed4 += 0.001;
			}
		}
		else if (x > tranx) {
			if (speed == 1) {
				if (speed1 >= 0.01) {
					speed1 -= 0.001;
				}
			}
			if (speed == 2) {
				if (speed2 >= 0.01) {
					speed2 -= 0.001;
				}
			}
			if (speed == 3) {
				if (speed3 >= 0.01) {
					speed3 -= 0.001;
				}
			}
			if (speed == 4) {
				if (speed4 >= 0.01) {
					speed4 -= 0.001;
				}
			}
		}
	}
	return vecter;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
GLvoid reshape(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, w / h, 100.0, 40000);
}
////////////////////////////////////////////////////////////////////////////////////////////////////
void Mouse(int button, int state, int x, int y)
{
	downx = x; downy = y;
	LeftButtonDown = ((button == GLUT_LEFT_BUTTON) && (state == GLUT_DOWN));
	RightButtonDown = ((button == GLUT_RIGHT_BUTTON) && (state == GLUT_DOWN));
	glutPostRedisplay();
}
////////////////////////////////////////////////////////////////////////////////////////////////////
void Motion(int x, int y)
{
	if (LeftButtonDown)
	{
		viewx += (float)(x - downx) / 4.0;
		viewy += (float)(downy - y) / 3.0;
	} // rotate
	if (RightButtonDown)
	{
		viewz += (float)(downy - y) * 10.0;
	} // scale
	downx = x;
	downy = y;
	glutPostRedisplay();
}
void BoyPerson()
{
	{
		glPushMatrix();
		glTranslatef(person.tranx, 0, person.tranz);
		glRotatef(person.roty, 0.0, 1.0, 0.0);
		glScalef(2, 2, 2);
		{
			glPushMatrix();
			//왼다리 허벅지
			glColor3f(0.0, 0.0, 1);//파랑색
			glTranslatef(-7.5, 90.0, 0.0);
			glRotatef(person_lleg_up.rotz, 1, 0, 0);
			glTranslatef(0.0, -25.0, 0.0);
			{
				glPushMatrix();
				glScalef(1.0, 3.0, 1.0);
				glutSolidCube(15);
				glPopMatrix();
			}
			//왼다리 종아리
			glColor3f(0.9, 0.6, 0.35);//살색
			glTranslatef(0.0, -15.0, 0.0);
			glRotatef(person_lleg_down.rotz, 1, 0, 0);
			glTranslatef(0.0, -30.0, 0.0);
			glScalef(0.6, 3.0, 0.6);
			glutSolidCube(15);
			glPopMatrix();
		}
		{
			glPushMatrix();
			//오른다리 허벅지
			glColor3f(0.0, 0.0, 1);//파랑색
			glTranslatef(7.5, 90.0, 0.0);
			glRotatef(person_rleg_up.rotz, 1, 0, 0);
			glTranslatef(0.0, -25.0, 0.0);
			{
				glPushMatrix();
				glScalef(1.0, 3.0, 1.0);
				glutSolidCube(15);
				glPopMatrix();
			}
			//오른다리 종아리
			glColor3f(0.9, 0.6, 0.35);//살색
			glTranslatef(0.0, -15.0, 0.0);
			glRotatef(person_rleg_down.rotz, 1, 0, 0);
			glTranslatef(0.0, -30.0, 0.0);
			glScalef(0.6, 3.0, 0.6);
			glutSolidCube(15);
			glPopMatrix();
		}
		{
			glPushMatrix();
			// 얼굴      
			glColor3f(0.9, 0.6, 0.35);//살색
			glTranslatef(0.0, 157, 0.0);
			glRotatef(person_head.rotz, 1, 0, 0);
			glTranslatef(0.0, 13.0, 0.0);
			glScalef(1.5, 2.0, 1.5);
			glutSolidSphere(6, 30, 30);
			glPopMatrix();
		}
		{
			glPushMatrix();
			//모가지
			glTranslatef(0.0, 157, 0.0);
			glRotatef(person_head.rotz, 1, 0, 0);
			glScalef(1.5, 3.5, 1.5);
			glutSolidCube(4);
			glPopMatrix();
		}
		{
			glPushMatrix();
			//몸통
			glColor3f(0.6, 0.6, 0.6);
			glTranslatef(0.0, 120, 0.0);
			glScalef(1.0, 2.0, 0.6);
			glutSolidCube(30);
			glPopMatrix();
		}
		{
			glPushMatrix();
			// 왼팔 팔 윗부분
			//glColor3f(1.0, 0.0, 0.0); // 수정중
			glColor3f(0.0, 1.0, 1.0);
			glTranslatef(-20.0, 150, 0.0);
			glRotatef(person_lhand_up.rotz, 1.0, 0.0, 0.0);
			glTranslatef(0.0, -20, 0.0);
			{
				glPushMatrix();
				glScalef(1.0, 4.0, 1.0);
				glutSolidCube(10);
				glPopMatrix();
			}// 왼팔 팔 아랫 부분      
			glColor3f(0.9, 0.6, 0.35);//살색
			glTranslatef(0.0, -20, 0.0);
			glRotatef(person_lhand_down.rotz, 1.0, 0.0, 0.0);
			glTranslatef(0.0, -20, 0.0);
			glScalef(1.0, 4.0, 0.8);
			glutSolidCube(10);
			glPopMatrix();
		}
		{
			glPushMatrix();
			// 오른팔 팔 윗부분
			glColor3f(1.0, 0.0, 0.0);
			glTranslatef(20.0, 150, 0.0);
			glRotatef(person_rhand_up.rotz, 1.0, 0.0, 0.0);
			glTranslatef(0.0, -20, 0.0);
			{
				glPushMatrix();
				glScalef(1.0, 4.0, 1.0);
				glutSolidCube(10);
				glPopMatrix();
			}
			// 오른팔 팔 아랫 부분
			glColor3f(0.9, 0.6, 0.35);//살색
			glTranslatef(0.0, -20, 0.0);
			glRotatef(person_rhand_down.rotz, 1.0, 0.0, 0.0);
			glTranslatef(0.0, -20, 0.0);
			glScalef(1.0, 4.0, 0.8);
			glutSolidCube(10);
			glPopMatrix();
		}
		glPopMatrix();
	}
}
///////////////////////////////////////////자이로드롭///////////////////////////////////////////////
void DrawGyroDrop(void)
{
	glPushMatrix();
	glTranslatef(-2000, 0, -2000);
	glScalef(2.5, 2.5, 2.5);
	{
		glPushMatrix();
		glColor3f(0.9, 0.9, 0.9);
		glRotatef(90, -1, 0, 0);
		SquareTower(0, 0, 0, 30, 1160);
		glPopMatrix();
	}
	{
		glPushMatrix();
		glColor3f(1, 0, 0);
		glTranslatef(0, 1160, 0);
		glRotatef(90, 1, 0, 0);
		glScalef(1, 1, 0.5);
		glutSolidCone(100, 200, 30, 30);
		glPopMatrix();
	}
	{
		glPushMatrix();
		glColor3f(1, 1, 1);
		glTranslatef(0, 1175, 0);
		glScalef(1, 0.5, 1);
		glutSolidSphere(100, 30, 30);
		glPopMatrix();
	}
	{
		glPushMatrix();
		glTranslatef(0, GyroDrop.tranz + 160, 0);
		glRotatef(GyroDrop.roty, 0, 1, 0);

		glRotatef(90, 1, 0, 0);
		glColor3f(1, 1, 0);
		glutSolidTorus(50, 100, 100, 100);
		glColor3f(0, 0, 1);
		//glutSolidCube(250);
		glPopMatrix();
	}
	glPopMatrix();
}
void DrawBigWheel(void)
{
	{
		{
			glPushMatrix();
			glTranslatef(0, 1450, 1000);
			glRotatef(BigWheel.rotz, 0, 0, 1);
			glScalef(3, 3, 3);
			glTranslatef(0, -550, 0);
			for (BigWheel.angle = 0.0f; BigWheel.angle < 360.0f; BigWheel.angle += 30)
			{
				glPushMatrix();
				glTranslatef(0, 550, 0);
				glTranslatef((cosf(BigWheel.angle*radian) * 400), (sinf(BigWheel.angle*radian) * 400), 0.0f);
				glRotatef(BigWheel.rotz, 0, 0, -1);
				glColor3f(0, 1, 1);
				//glutSolidCube(100);
				SquareTower(0, 0, 0, 40, 100);
				glPopMatrix();
			}
			glPopMatrix();
		}
		glPushMatrix();
		glTranslatef(0, 1450, 1000);
		glRotatef(BigWheel.rotz, 0, 0, 1);
		glScalef(3, 3, 3);
		glTranslatef(0, -550, 0);
		//////////////////////////////////////////관람 통////////////////////////////////////////////
		//////////////////////////////////////////중간 틀////////////////////////////////////////////
		{
			glPushMatrix();
			glTranslatef(0, 0, 50);
			glColor3f(1, 0, 1);
			{
				glPushMatrix();
				glTranslatef(0, 950, 0);
				glRotatef(90, 1, 0, 0);
				SquareTower(0, 0, 0, 3, 800);
				{
					glPushMatrix();
					glTranslated(-370, 0, 150);
					glRotatef(30, 0, 1, 0);
					glTranslated(200, 0, 0);
					SquareTower(0, 0, 0, 3, 800);
					glPopMatrix();
				}
				{
					glPushMatrix();
					glTranslated(20, 0, -50);
					glRotatef(30, 0, -1, 0);
					glTranslated(200, 0, 0);
					SquareTower(0, 0, 0, 3, 800);
					glPopMatrix();
				}
				{
					glPushMatrix();
					glTranslated(-350, 0, 190);
					glRotatef(60, 0, 1, 0);
					SquareTower(0, 0, 0, 3, 800);
					glPopMatrix();
				}
				{
					glPushMatrix();
					glTranslated(350, 0, 190);
					glRotatef(-60, 0, 1, 0);
					SquareTower(0, 0, 0, 3, 800);
					glPopMatrix();
				}
				{
					glPushMatrix();
					glTranslatef(-400, 0, 390);
					glRotatef(90, 0, 1, 0);
					SquareTower(0, 0, 0, 3, 800);
					glTranslatef(400, 0, 0);
					glPopMatrix();
				}
				glPopMatrix();
			}
			glPopMatrix();
		}
		//////////////////////////////////////////통묶는 토러스////////////////////////////////////////////
		{
			glPushMatrix();
			glTranslatef(0, 550, 50);
			glutSolidTorus(3, 430, 100, 100);
			glPopMatrix();
		}
		{
			glPushMatrix();
			glTranslatef(0, 550, 50);
			glutSolidTorus(3, 230, 100, 100);
			glPopMatrix();
		}
		glPopMatrix();
		/////////////////////////////////////////////대관람차 고정틀/////////////////////////////////////////////////////
		glColor3f(1, 0, 0);
		glPushMatrix();
		glTranslatef(0, -280, 1040);
		glRotatef(90, 0, 1, 0);
		glScalef(2, 2, 2);
		{
			glPushMatrix();
			glTranslatef(200, 850, 0);
			glRotatef(70, 1, 0, 0);
			SquareTower(0, 0, 0, 30, 800);
			glPopMatrix();
		}
		{
			glPushMatrix();
			glTranslatef(200, 100, -300);
			glRotatef(70, -1, 0, 0);
			SquareTower(0, 0, 0, 30, 800);
			glPopMatrix();
		}
		{
			glPushMatrix();
			glTranslatef(-200, 850, 0);
			glRotatef(70, 1, 0, 0);
			SquareTower(0, 0, 0, 30, 800);
			glPopMatrix();
		}
		{
			glPushMatrix();
			glTranslatef(-200, 100, -300);
			glRotatef(70, -1, 0, 0);
			SquareTower(0, 0, 0, 30, 800);
			glPopMatrix();
		}
		{
			glPushMatrix();
			glTranslatef(-228, 880, -15);
			glRotatef(90, 0, 1, 0);
			SquareTower(0, 0, 0, 50, 455);
			glPopMatrix();
		}
		glPopMatrix();
	}
}
//////////////////////////////////////////////바닥////////////////////////////////////////////////////
void DrawFloor(void)
{
	glPushMatrix();

	glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);
	glColor3f(1, 1,1);
	glBindTexture(GL_TEXTURE_2D, textures[3]); // 앞
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0, 0);
		glVertex3f(-8000, 0, -8000);
		glTexCoord2f(0, 1);
		glVertex3f(-8000, 0, 8000);
		glTexCoord2f(1, 1);
		glVertex3f(8000, 0, 8000);
		glTexCoord2f(1, 0);
		glVertex3f(8000, 0, -8000);
	}
	glEnd();

	glBindTexture(GL_TEXTURE_2D, textures[4]);
	glBegin(GL_QUADS);//왼쪽 옆면
	{
		glTexCoord2f(1, 1);
		glVertex3f(-8000,8000, 8000);
		glTexCoord2f(1, 0);
		glVertex3f(-8000, 0, 8000);
		glTexCoord2f(0, 0);
		glVertex3f(-8000, 0, -8000);
		glTexCoord2f(0, 1);
		glVertex3f(-8000, 8000, -8000);

	}
	glEnd();
	glBindTexture(GL_TEXTURE_2D, textures[5]);
	glBegin(GL_QUADS);//뒷면
	{
		glTexCoord2f(1, 1);
		glVertex3f(-8000, 8000, -8000);
		glTexCoord2f(1, 0);
		glVertex3f(-8000, 0, -8000);
		glTexCoord2f(0, 0);
		glVertex3f(8000, 0, -8000);
		glTexCoord2f(0, 1);
		glVertex3f(8000, 8000, -8000);
	}
	glEnd();

	glBindTexture(GL_TEXTURE_2D, textures[6]);
	glBegin(GL_QUADS);
	{
		glTexCoord2f(0, 1);
		glVertex3f(8000, 8000, -8000);
		glTexCoord2f(0, 0);
		glVertex3f(8000, 0, -8000);
		glTexCoord2f(1, 0);
		glVertex3f(8000, 0, 8000);
		glTexCoord2f(1, 1);
		glVertex3f(8000, 8000, 8000);
	}
	glEnd();

	glBindTexture(GL_TEXTURE_2D, textures[2]);//윗면
	glBegin(GL_QUADS);
	{
		glTexCoord2f(1, 0);
		glVertex3f(8000, 8000, -8000);
		glTexCoord2f(1, 1);
		glVertex3f(8000, 8000, 8000);
		glTexCoord2f(0, 1);
		glVertex3f(-8000, 8000, 8000);
		glTexCoord2f(0, 0);
		glVertex3f(-8000, 8000, -8000);
	}
	glEnd();

	glPushMatrix();
	glTranslatef(-titlex, titley, 0);
	glBindTexture(GL_TEXTURE_2D, textures[8]);
	glBegin(GL_QUADS);//왼쪽위
	{
		glTexCoord2f(1, 1);
		glVertex3f(-8000, 8000, 8000);
		glTexCoord2f(1, 0);
		glVertex3f(-8000, 4000, 8000);
		glTexCoord2f(0, 0);
		glVertex3f(0, 4000, 8000);
		glTexCoord2f(0, 1);
		glVertex3f(0, 8000, 8000);
	}
	glEnd();
	glPopMatrix();
	glPushMatrix();

	glTranslatef(titlex, titley, 0);
	glBindTexture(GL_TEXTURE_2D, textures[7]);
	glBegin(GL_QUADS);//오른쪽위
	{
		glTexCoord2f(1, 1);
		glVertex3f(0, 8000, 8000);
		glTexCoord2f(1, 0);
		glVertex3f(0, 4000, 8000);
		glTexCoord2f(0, 0);
		glVertex3f(8000, 4000, 8000);
		glTexCoord2f(0, 1);
		glVertex3f(8000, 8000, 8000);
	}
	glEnd();
	glPopMatrix();
	glPushMatrix();
	glTranslatef(-titlex, -titley, 0);
	glBindTexture(GL_TEXTURE_2D, textures[10]);
	glBegin(GL_QUADS);//왼쪽아래
	{

		glTexCoord2f(1, 1);
		glVertex3f(-8000, 4000, 8000);
		glTexCoord2f(1, 0);
		glVertex3f(-8000, 0, 8000);
		glTexCoord2f(0, 0);
		glVertex3f(0, 0, 8000);
		glTexCoord2f(0, 1);
		glVertex3f(0, 4000, 8000);
	}
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(titlex, -titley, 0);
	glBindTexture(GL_TEXTURE_2D, textures[9]);
	glBegin(GL_QUADS);//오른쪽아래
	{
		glTexCoord2f(1, 1);
		glVertex3f(0, 4000, 8000);
		glTexCoord2f(1, 0);
		glVertex3f(0, 0, 8000);
		glTexCoord2f(0, 0);
		glVertex3f(8000, 0, 8000);
		glTexCoord2f(0, 1);
		glVertex3f(8000, 4000, 8000);
	}
	glEnd();
	glPopMatrix();
	glDisable(GL_CULL_FACE);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

}
void SquareTower(GLfloat centerx, GLfloat centery, GLfloat centerz, GLfloat radius, GLfloat h)
{
	/* function createCyliner()
	원기둥의 중심 x,y,z좌표, 반지름, 높이를 받아 원기둥을 생성하는 함수(+z방향으로 원에서 늘어남)
	centerx : 원기둥 원의 중심 x좌표
	centery : 원기둥 원의 중심 y좌표
	centerz : 원기둥 원의 중심 z좌표
	radius : 원기둥의 반지름
	h : 원기둥의 높이
	*/
	GLfloat x, y, angle;
	glBegin(GL_TRIANGLE_FAN);           //원기둥의 윗면
	glNormal3f(0.0f, 0.0f, -1.0f);
	glVertex3f(centerx, centery, centerz);
	for (angle = 0.0f; angle < (2.0f*GL_PI); angle += (GL_PI / 8.0f))
	{
		x = centerx + radius*sin(angle);
		y = centery + radius*cos(angle);
		glNormal3f(0.0f, 0.0f, -1.0f);
		glVertex3f(x, y, centerz);
	}
	glEnd();
	glBegin(GL_QUAD_STRIP);            //원기둥의 옆면
	for (angle = 0.0f; angle < (2.0f*GL_PI); angle += (GL_PI / 8.0f))
	{
		x = centerx + radius*sin(angle);
		y = centery + radius*cos(angle);
		glNormal3f(sin(angle), cos(angle), 0.0f);
		glVertex3f(x, y, centerz);
		glVertex3f(x, y, centerz + h);
	}
	glEnd();
	glBegin(GL_TRIANGLE_FAN);           //원기둥의 밑면
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(centerx, centery, centerz + h);
	for (angle = (2.0f*GL_PI); angle > 0.0f; angle -= (GL_PI / 8.0f))
	{
		x = centerx + radius*sin(angle);
		y = centery + radius*cos(angle);
		glNormal3f(0.0f, 0.0f, 1.0f);
		glVertex3f(x, y, centerz + h);
	}
	glEnd();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////+

void calcNormal(float v[3][3], float out[3])
{
	/* function calcNormal()
	: 점 3개를 받아 해당 삼각형의 normal vector를 구하는 함수
	v[3][3] : (x, y, z) 점 좌표 3개
	out[3] : normal 벡터를 반환받을 벡터 3개
	※ glut함수를 쓰면 느리므로 직접 제작
	*/
	float v1[3], v2[3], length;
	static const int x = 0;
	static const int y = 1;
	static const int z = 2;
	v1[x] = v[0][x] - v[1][x]; v1[y] = v[0][y] - v[1][y]; v1[z] = v[0][z] - v[1][z];
	v2[x] = v[2][x] - v[1][x]; v2[y] = v[2][y] - v[1][y]; v2[z] = v[2][z] - v[1][z];
	out[x] = v1[y] * v2[z] - v1[z] * v2[y];
	out[y] = v1[z] * v2[x] - v1[x] * v2[z];
	out[z] = v1[x] * v2[y] - v1[y] * v2[x];
	length = (float)sqrt(out[x] * out[x] + out[y] * out[y] + out[z] * out[z]);
	if (length == 0.0f)
		length = 1.0f;
	out[x] /= length;
	out[y] /= length;
	out[z] /= length;
}
void CreateShip(GLfloat x, GLfloat y, GLfloat z, GLfloat size)
{
	/* function createShip()
	중심 (x,y,z)로 부터 z축으로 길게 배 생성, 옆면 길이는 size * 2, 총 길이는 size * 4
	x : x 좌표
	y : y 좌표
	z : z 좌표
	size : 배 크기의 기본단위
	*/
	float v[3][3];
	float out[3];
	glBegin(GL_TRIANGLES);
	glColor3f(1, 0.7, 0.0);   //바이킹 뒷 왼쪽 삼각형
	v[0][0] = x - size; v[0][1] = y; v[0][2] = z - size;
	v[1][0] = x; v[1][1] = y; v[1][2] = z - 2 * size;
	v[2][0] = x; v[2][1] = y - size; v[2][2] = z - size;
	calcNormal(v, out);
	glNormal3fv(out);
	glVertex3f(x - size, y, z - size);
	glVertex3f(x, y, z - 2 * size);
	glVertex3f(x, y - size, z - size);
	v[0][0] = x; v[0][1] = y - size; v[0][2] = z + size;
	v[1][0] = x; v[1][1] = y; v[1][2] = z - 2 * size;
	v[2][0] = x - size; v[2][1] = y; v[2][2] = z + size;
	calcNormal(v, out);
	glNormal3fv(out);
	glVertex3f(x, y - size, z + size);  //바이킹 앞 왼쪽 삼각형
	glVertex3f(x, y, z + 2 * size);
	glVertex3f(x - size, y, z + size);
	v[0][0] = x; v[0][1] = y - size; v[0][2] = z + size;
	v[1][0] = x - size; v[1][1] = y; v[1][2] = z - size;
	v[2][0] = x; v[2][1] = y - size; v[2][2] = z - size;
	calcNormal(v, out);
	glNormal3fv(out);
	glVertex3f(x, y - size, z + size);
	glVertex3f(x - size, y, z - size);
	glVertex3f(x, y - size, z - size);
	v[0][0] = x - size; v[0][1] = y; v[0][2] = z - size;
	v[1][0] = x; v[1][1] = y - size; v[1][2] = z + size;
	v[2][0] = x - size; v[2][1] = y; v[2][2] = z + size;
	calcNormal(v, out);
	glNormal3fv(out);
	glVertex3f(x - size, y, z - size);
	glVertex3f(x, y - size, z + size);
	glVertex3f(x - size, y, z + size);
	v[0][0] = x; v[0][1] = y; v[0][2] = z - 2 * size;
	v[1][0] = x + size; v[1][1] = y; v[1][2] = z - size;
	v[2][0] = x; v[2][1] = y - size; v[2][2] = z - size;
	calcNormal(v, out);
	glNormal3fv(out);
	glVertex3f(x, y, z - 2 * size);
	glVertex3f(x + size, y, z - size);
	glVertex3f(x, y - size, z - size);
	v[0][0] = x; v[0][1] = y - size; v[0][2] = z + size;
	v[1][0] = x + size; v[1][1] = y; v[1][2] = z + size;
	v[2][0] = x; v[2][1] = y; v[2][2] = z + 2 * size;
	calcNormal(v, out);
	glNormal3fv(out);
	glVertex3f(x, y - size, z + size);  //바이킹 앞 오른쪽 삼각형
	glVertex3f(x + size, y, z + size);
	glVertex3f(x, y, z + 2 * size);

	v[0][0] = x + size; v[0][1] = y; v[0][2] = z - size;
	v[1][0] = x; v[1][1] = y - size; v[1][2] = z + size;
	v[2][0] = x; v[2][1] = y - size; v[2][2] = z - size;
	calcNormal(v, out);
	glNormal3fv(out);
	glVertex3f(x + size, y, z - size);
	glVertex3f(x, y - size, z + size);
	glVertex3f(x, y - size, z - size);
	v[0][0] = x + size; v[0][1] = y; v[0][2] = z - size;
	v[1][0] = x + size; v[1][1] = y; v[1][2] = z + size;
	v[2][0] = x; v[2][1] = y - size; v[2][2] = z + size;
	calcNormal(v, out);
	glNormal3fv(out);
	glVertex3f(x + size, y, z - size);
	glVertex3f(x + size, y, z + size);
	glVertex3f(x, y - size, z + size);
	glEnd();
	glBegin(GL_POLYGON);     //바이킹 갑판
	glColor3f(1, 0.6, 0);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(x + size, y, z - size);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(x, y, z - 2 * size);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(x - size, y, z - size);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(x - size, y, z + size);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(x, y, z + 2 * size);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(x + size, y, z + size);
	glEnd();
}
////////////////////////////////////////////////////////////////////////////////////////////////////
void DrawViking(void)
{
	{
		glPushMatrix();
		glTranslatef(-2000, -200, 2000);
		glScalef(2, 2, 2);
		glColor3f(1, 0, 1);
		{
			glPushMatrix();
			glTranslatef(200, 850, 0);
			glRotatef(70, 1, 0, 0);
			SquareTower(0, 0, 0, 30, 800);
			glPopMatrix();
		}
		{
			glPushMatrix();
			glTranslatef(200, 100, -300);
			glRotatef(70, -1, 0, 0);
			SquareTower(0, 0, 0, 30, 800);
			glPopMatrix();
		}
		{
			glPushMatrix();
			glTranslatef(-200, 850, 0);
			glRotatef(70, 1, 0, 0);
			SquareTower(0, 0, 0, 30, 800);
			glPopMatrix();
		}
		{
			glPushMatrix();
			glTranslatef(-200, 100, -300);
			glRotatef(70, -1, 0, 0);
			SquareTower(0, 0, 0, 30, 800);
			glPopMatrix();
		}
		/////////////////////////////////////////////////////////////////////////////바이킹 고정 틀/////////////////////////
		glPushMatrix();
		glTranslatef(0, 850, 0);
		glRotatef(Viking.rotx, 1, 0, 0);
		glTranslatef(0, -650, 0);
		{
			glPushMatrix();
			glTranslatef(0, 100, 0);

			CreateShip(0, 0, 0, 140);
			/////////////////////////////////////////////
			///////////////////////////////////////
			{
				glPushMatrix();
				glColor3f(0, 1, 1);
				glTranslatef(-225, 550, -10);
				glRotatef(90, 0, 1, 0);
				SquareTower(0, 0, 0, 40, 450);
				{
					glPushMatrix();
					glTranslated(0, 0, 100);
					glRotatef(90, 1, 0, 0);
					SquareTower(0, 0, 0, 20, 550);
					glPopMatrix();
				}
				{
					glPushMatrix();
					glTranslated(0, 0, 350);
					glRotatef(90, 1, 0, 0);
					SquareTower(0, 0, 0, 20, 550);
					glPopMatrix();
				}
				glPopMatrix();
			}////////////////////////////////////////

			glPopMatrix();
		}
		glPopMatrix();
	}
	glPopMatrix();
}

void DrawHorse()//말 만드는 함수
{
	{
		glPushMatrix();
		glColor4f(1, 0.5, 0.1, 0.1);
		glScalef(0.2, 0.2, 0.2);
		{
			glPushMatrix();//몸통
			glScalef(2.6, 1.3, 1);
			glutSolidSphere(100, 30, 30);
			{
				glPushMatrix();///////꼬리
				glTranslatef(-100, 30, 0);
				glRotatef(-20, 0, 0, 1);
				glutSolidSphere(30, 30, 30);
				{
					glPushMatrix();///////꼬리
					glTranslatef(-30, -40, 0);
					glutSolidSphere(30, 30, 30);
					{
						glPushMatrix();///////꼬리
						glTranslatef(-20, -40, 0);
						glutSolidSphere(30, 30, 30);
						glPopMatrix();
					}   glPopMatrix();
				}   glPopMatrix();
			}
			glPopMatrix();
		}

		{
			glPushMatrix();///////몸통이랑 이어지는 목
			glTranslatef(240, 130, 0);
			glRotatef(-15, 0, 0, 1);
			glScalef(1, 2, 1);
			glutSolidSphere(70, 30, 30);
			glPopMatrix();
		}
		{
			glPushMatrix();//목이랑 이어지는 머리
			glTranslatef(330, 200, 0);
			glRotatef(40, 0, 0, 1);
			glScalef(1, 2, 1);
			glutSolidSphere(65, 30, 30);
			glPopMatrix();
		}
		{
			glPushMatrix();//머리에 뿔
			glColor3f(1, 1, 0);
			glTranslatef(330, 280, 0);
			glRotatef(90, -1, 0.5, 0);
			glutSolidCone(20, 200, 30, 30);
			glPopMatrix();
		}
		///////////////////////////////////////////////////////////////////
		{
			glColor4f(1, 0.5, 0.1, 0.1);
			glPushMatrix();//앞다리 윗쪽 왼쪽
			glTranslatef(80, -95, -80);
			glRotatef(90, 0, 1, 0);
			SquareTower(0, 0, 0, 50, 215);
			glPopMatrix();
		}
		{
			glPushMatrix();//앞다리 윗쪽 오른쪽
			glTranslatef(80, -95, 80);
			glRotatef(90, 0, 1, 0);
			SquareTower(0, 0, 0, 50, 215);
			glPopMatrix();
		}
		{
			glPushMatrix();//앞다리 아랫쪽 왼쪽
			glTranslatef(270, -95, -80);
			glRotatef(90, 1, -0.1, 0);
			SquareTower(0, 0, 0, 30, 215);
			glPopMatrix();
		}
		{
			glPushMatrix();//앞다리 아랫쪽 왼쪽
			glTranslatef(270, -95, 80);
			glRotatef(90, 1, -0.1, 0);
			SquareTower(0, 0, 0, 30, 215);
			glPopMatrix();
		}
		{   ////////////////////////////////////////////
			glPushMatrix();//앞다리 왼쪽 발굽
			glTranslatef(270, -280, -80);
			glRotatef(90, 0.5, -1, 0);
			SquareTower(0, 0, 0, 30, 170);
			glPopMatrix();
		}
		{
			glPushMatrix();//앞다리 오른쪽 발굽
			glTranslatef(270, -280, 80);
			glRotatef(90, 0.5, -1, 0);
			SquareTower(0, 0, 0, 30, 170);
			glPopMatrix();
		}
		////////////////////////////////////////////////////////////
		{
			glPushMatrix();//뒷다리
			glTranslatef(-300, -95, 80);
			glRotatef(90, 0, 1, 0);
			SquareTower(0, 0, 0, 55, 215);
			glPopMatrix();
		}
		{
			glPushMatrix();//뒷다리 
			glTranslatef(-300, -95, -80);
			glRotatef(90, 0, 1, 0);
			SquareTower(0, 0, 0, 55, 215);
			glPopMatrix();
		}
		{
			glPushMatrix();//뒷다리 아랫쪽 왼쪽
			glTranslatef(-285, -95, -80);
			glRotatef(90, 1, 0, 0);
			SquareTower(0, 0, 0, 30, 215);
			glPopMatrix();
		}
		{
			glPushMatrix();//뒷다리 아랫쪽 왼쪽
			glTranslatef(-285, -95, 80);
			glRotatef(90, 1, 0, 0);
			SquareTower(0, 0, 0, 30, 215);
			glPopMatrix();
		}
		{
			glPushMatrix();//뒷다리 왼쪽 발굽
			glTranslatef(-285, -280, -80);
			glRotatef(90, 0.65, -1, 0);
			SquareTower(0, 0, 0, 30, 150);
			glPopMatrix();
		}
		{
			glPushMatrix();//뒷다리 오른쪽 발굽
			glTranslatef(-285, -280, 80);
			glRotatef(90, 0.65, -1, 0);
			SquareTower(0, 0, 0, 30, 150);
			glPopMatrix();
		}
		glPopMatrix();
	}
}
void DrawMerryGoRound(void)
{
	glPushMatrix();
	glTranslatef(2000, 20, -2000);
	glRotatef(MerryGoRound.roty, 0, 1, 0);
	glScalef(0.9, 0.9, 0.9);
	///////////////////////////////////////////////바닥3개 ////////////////////////////////////////////////////////
	{
		glPushMatrix();
		glColor3f(1, 0, 0.8);
		glRotatef(90, 1, 0, 0);
		SquareTower(0, 0, 0, 550, 20);//바닥
		glColor3f(1, 1, 0);
		glTranslatef(0, 0, -20);
		SquareTower(0, 0, 0, 500, 20);//바닥
		glColor3f(0, 1, 1);
		glTranslatef(0, 0, -20);
		SquareTower(0, 0, 0, 450, 20);//바닥
		glPopMatrix();
	}
	///////////////////////////////////////////////지붕////////////////////////////////////////////////////////
	{
		glPushMatrix();
		glColor3f(1, 0, 0.8);
		glTranslatef(0, 400, 0);
		glRotatef(90, -1, 0, 0);
		glutSolidCone(500, 200, 30, 30);
		glTranslatef(0, 0, 180);
		glColor3f(1, 1, 0);
		glutSolidCone(50, 80, 30, 30);
		glTranslatef(0, 0, 130);
		glColor3f(0, 1, 1);
		glutSolidSphere(40, 15, 30);
		glPopMatrix();
	}
	///////////////////////////////////////////////지붕 처마에 원뿔이랑 원////////////////////////////////////////////////////////
	{
		glPushMatrix();
		for (MerryGoRound.angle = 0.0f; MerryGoRound.angle < 360.0f; MerryGoRound.angle += 30)
		{
			glPushMatrix();
			glTranslatef(0, 400, 0);
			glRotatef(90, -1, 0, 0);
			glTranslatef((cosf(MerryGoRound.angle*radian) * 480), (sinf(MerryGoRound.angle*radian) * 480), 0.0f);
			glColor3f(1, 1, 0);
			glutSolidCone(20, 50, 30, 30);
			glColor3f(0, 1, 1);
			glTranslatef(0, 0, 50);
			glutSolidSphere(10, 30, 30);
			glPopMatrix();
		}
		glPopMatrix();
	}
	///////////////////////////////////////////////말 고정 기둥////////////////////////////////////////////////////////
	{
		glPushMatrix();
		for (MerryGoRound.angle = 0.0f; MerryGoRound.angle < 360.0f; MerryGoRound.angle += 60)
		{
			glPushMatrix();
			glTranslatef(0, 400, 0);
			glRotatef(90, 1, 0, 0);
			glColor3f(0.0, 1, 1);
			glTranslatef((cosf(MerryGoRound.angle*radian) * 400), (sinf(MerryGoRound.angle*radian) * 400), 0.0f);
			SquareTower(0, 0, 0, 5, 350);
			glPopMatrix();
		}
		glPopMatrix();
	}
	{
		glPushMatrix();
		for (MerryGoRound.angle = 30.0f; MerryGoRound.angle < 360.0f; MerryGoRound.angle += 60)
		{
			glPushMatrix();
			glTranslatef(0, 400, 0);
			glRotatef(90, 1, 0, 0);
			glColor3f(1, 1, 0);
			glTranslatef((cosf(MerryGoRound.angle*radian) * 300), (sinf(MerryGoRound.angle*radian) * 300), 0.0f);
			SquareTower(0, 0, 0, 5, 350);
			glPopMatrix();
		}
		glPopMatrix();
	}
	//////////////////////////////////////////////말만/////////////////////////////////////////
	{
		glPushMatrix();
		glTranslatef(0, MerryGoRound.trany, 0);
		for (MerryGoRound.angle = 0.0f; MerryGoRound.angle < 360.0f; MerryGoRound.angle += 60)
		{
			glPushMatrix();
			glTranslatef(0, 400, 0);
			glRotatef(90, 1, 0, 0);
			glColor3f(0.0, 1, 1);
			glTranslatef((cosf(MerryGoRound.angle*radian) * 400), (sinf(MerryGoRound.angle*radian) * 400), 0.0f);
			glTranslatef(0, 0, 250);
			glRotatef(90, -1, 0, 0);
			glRotatef(MerryGoRound.roty, 0, -1, 0);
			DrawHorse();
			glPopMatrix();
		}
		glPopMatrix();
	}
	{
		glPushMatrix();
		glTranslatef(0, -MerryGoRound.trany, 0);
		for (MerryGoRound.angle = 30.0f; MerryGoRound.angle < 360.0f; MerryGoRound.angle += 60)
		{
			glPushMatrix();
			glTranslatef(0, 400, 0);
			glRotatef(90, 1, 0, 0);
			glColor3f(1, 1, 0);
			glTranslatef((cosf(MerryGoRound.angle*radian) * 300), (sinf(MerryGoRound.angle*radian) * 300), 0.0f);
			glTranslatef(0, 0, 200);
			glRotatef(90, -1, 0, 0);
			glRotatef(MerryGoRound.roty, 0, -1, 0);
			DrawHorse();
			glPopMatrix();
		}
		glPopMatrix();
	}
	glPopMatrix();
	///////////////////////////////////////////////바깥 팬스////////////////////////////////////////////////////////
	{
		glPushMatrix();
		glTranslatef(2000, 50, -2000);
		glScalef(0.75, 0.75, 0.75);
		glPushMatrix();
		for (MerryGoRound.angle = 30.0f; MerryGoRound.angle < 360.0f; MerryGoRound.angle += 4)
		{
			glPushMatrix();
			glTranslatef(0, 100, 0);
			glRotatef(90, 1, 0, 0);
			glColor3f(0.3, 0.3, 0.4);
			glTranslatef((cosf(MerryGoRound.angle*radian) * 550), (sinf(MerryGoRound.angle*radian) * 550), 0.0f);
			SquareTower(0, 0, 0, 2, 150);
			glPopMatrix();
		}
		glRotatef(90, -1, 0, 0);
		glutSolidTorus(5, 550, 30, 30);
		glTranslatef(0, 0, 100);
		glutSolidTorus(7, 550, 30, 30);
		glTranslatef(0, 0, -50);
		glutSolidTorus(5, 550, 30, 30);
		glPopMatrix();
	}
	glPopMatrix();
}
////////////////////////////////////////////////////////////////////////////////////////////////////
GLubyte* LoadDIBitmap(const char *filename, BITMAPINFO **info)
{
	FILE *fp;
	GLubyte *bits;
	int bitsize, infosize;
	BITMAPFILEHEADER header;
	// 바이너리 읽기 모드로 파일을 연다
	if ((fp = fopen(filename, "rb")) == NULL)
		return NULL;
	// 비트맵 파일 헤더를 읽는다.
	if (fread(&header, sizeof(BITMAPFILEHEADER), 1, fp) < 1) {
		fclose(fp);
		return NULL;
	}
	// 파일이 BMP 파일인지 확인한다.
	if (header.bfType != 'MB') {
		fclose(fp);
		return NULL;
	}
	// BITMAPINFOHEADER 위치로 간다.
	infosize = header.bfOffBits - sizeof(BITMAPFILEHEADER);
	// 비트맵 이미지 데이터를 넣을 메모리 할당을 한다.
	if ((*info = (BITMAPINFO *)malloc(infosize)) == NULL) {
		fclose(fp);
		exit(0);
		return NULL;
	}
	// 비트맵 인포 헤더를 읽는다.
	if (fread(*info, 1, infosize, fp) < (unsigned int)infosize) {
		free(*info);
		fclose(fp);
		return NULL;
	}
	// 비트맵의 크기 설정
	if ((bitsize = (*info)->bmiHeader.biSizeImage) == 0)
		bitsize = ((*info)->bmiHeader.biWidth *
		(*info)->bmiHeader.biBitCount + 7) / 8.0 *
		abs((*info)->bmiHeader.biHeight);
	// 비트맵의 크기만큼 메모리를 할당한다.
	if ((bits = (unsigned char *)malloc(bitsize)) == NULL) {
		free(*info);
		fclose(fp);
		return NULL;
	}
	// 비트맵 데이터를 bit(GLubyte 타입)에 저장한다.
	if (fread(bits, 1, bitsize, fp) < (unsigned int)bitsize) {
		free(*info); free(bits);
		fclose(fp);
		return NULL;
	}
	fclose(fp);
	return bits;
}
//먼저 올라가는 사각형
void FireCracker_First() {
	glPushMatrix();
	if (firecracker_first.tf == false) {
		glColor3f(firecracker_color.colorr, firecracker_color.colorg, firecracker_color.colorb);
		glTranslatef(firecracker_first.tranx, firecracker_first.trany, firecracker_first.tranz); // x, z는 랜덤 
		glutSolidCube(100);
	}
	else {
		FireCracker_Second(firecracker_first.tranx, firecracker_first.trany, firecracker_first.tranz);
	}
	glPopMatrix();
}
void FireCracker_Second(float spot_x, float spot_y, float spot_z) {
	glPushMatrix();
	for (int i = 0; i <= 360; i += 30) {
		glPushMatrix();
		glColor3f(firecracker_color.colorr, firecracker_color.colorg, firecracker_color.colorb);
		glRotatef(i, 0, 1, 0);
		for (int j = 0; j <= 360; j += 30) {
			glPushMatrix();
			if (firecracker_second.tf == false) {
				glTranslatef(firecracker_r * cos(j * firecracker_randi) + spot_x, firecracker_r * sin(j * firecracker_randi) + spot_y, spot_z);
				glutSolidCube(50);
			}
			else {
				FireCracker_Second(firecracker_r * cos(j * firecracker_randi) + spot_x, firecracker_r * sin(j * firecracker_randi) + spot_y, spot_z);
			}
			glPopMatrix();
		}
		glPopMatrix();
	}
	glPopMatrix();
}
void FireCracker_Timer(int value) {
	if (firecracker_first.tf == false) {
		firecracker_first.trany += 100;

		if (firecracker_first.trany > 6000) {
			firecracker_first.tf = true;
		}
	}
	if (firecracker_first.tf == true) {
		firecracker_r += 60;
		if (firecracker_r > 4000) {
			firecracker_r = 0;
			firecracker_first.tranx = rand() % 10000 - 5000;
			firecracker_first.trany = 0;
			firecracker_first.tranz = rand() % 10000 - 5000;
			firecracker_color.colorr = (rand() % 10) + 7 * 0.1;
			firecracker_color.colorg = (rand() % 5) * 0.1;
			firecracker_color.colorb = rand() % 2 * 0.1;
			firecracker_first.tf = false;
			firecracker_second.tf = false;
		}
	}

	glutPostRedisplay();
	glutTimerFunc(10, FireCracker_Timer, 1);
}

void FireCracker_Set() {
	firecracker_first.tranx = rand() % 10000 - 5000;
	firecracker_first.trany = 0;
	firecracker_first.tranz = rand() % 10000 - 5000;
	firecracker_color.colorr = (rand() % 10)+8 * 0.1;
	firecracker_color.colorg = (rand() % 5) * 0.1;
	firecracker_color.colorb = rand() % 2 * 0.1;
}

//충돌 값 

// 수정
void Collise(float x1, float y1, float x2, float y2, float tranx, float tranz, int point) {  // x1값 오른쪽 / x2값 왼쪽 / y1값 위쪽 / y2값 아래쪽
	if (x1 + tranx > person.tranx) {
	}
	else if (x2 + tranx < person.tranx) {
	}
	else if (y1 + tranz > person.tranz) {
	}
	else if (y2 + tranz < person.tranz) {
	}
	else {
		printf("collise : %d \n", point);
		person.tranx = 0;
		person.tranz = 0;
		if (point == 0) { Viking.rotx = 0; collise_timer = 0, anglev = -100; }
		if (point == 1) { GyroDrop.tranz = 0; GyroDrop.roty = 0; }
		if (point == 2) { BigWheel.rotz = -90; }
		if (point == 3) { MerryGoRound.roty = 0; collise_timer = 0; }
		if (point == 4) { timer = 0.0; timer1 = 0.2; timer2 = 0.4; timer3 = 0.6; rollercoaster_counter = 0; }
		collise_judge[point] = true;
	}
}

void Collise_BB(float x1, float z1, float x2, float z2, float tranx, float tranz) {
	glColor3f(1.0, 1.0, 0.0);


	glLineWidth(10);
	glBegin(GL_LINE_STRIP);

	glVertex3f(x1 + tranx, 0, z1 + tranz);
	glVertex3f(x2 + tranx, 0, z1 + tranz);

	glVertex3f(x2 + tranx, 0, z1 + tranz);
	glVertex3f(x2 + tranx, 0, z2 + tranz);

	glVertex3f(x1 + tranx, 0, z2 + tranz);
	glVertex3f(x2 + tranx, 0, z2 + tranz);

	glVertex3f(x1 + tranx, 0, z1 + tranz);
	glVertex3f(x1 + tranx, 0, z2 + tranz);

	glEnd();
}

void Collise_Timer(int value) {

	// 바이킹
	if (collise_judge[0] == true) {
		collise_timer += 0.025;
		if (collise_timer > 5) {
			collise_judge[0] = false;
		}
	}

	// 회전목마
	if (collise_judge[3] == true) {
		collise_timer += 0.01;
		if (collise_timer > 5) {
			collise_judge[3] = false;
		}
	}

	if (collise_judge[4] == true) 
	{
		collise_timer += 0.0001;
		if (collise_timer > 5) {
			collise_judge[4] = false;
		}

	}

	glutTimerFunc(40, Collise_Timer, 1);
	glutPostRedisplay();
}